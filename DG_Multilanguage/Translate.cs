﻿using DG_Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;

namespace DG_Multilanguage
{
    public class Translate
    {
        private tanslations trans = new tanslations();

        public string Lang { get; set; } = "es";

        public Translate(string progDataFolder, string defaultLang)
        {
            try
            {
                Lang = defaultLang;
                //get current language
                string region = defaultLang;
                trans = GenericosDG.XmlFileToObj<tanslations>(@"Translations.xml");

            }
            catch (Exception ex)
            {
                GenericosDG.Add2DebugLog(ex.ToString());
            }
        }

        enum capitalization { allLower, capIni, allCaps };

        public string TranslateMsg(string msg)
        {
            string transMsg;

            capitalization c = capitalization.allLower;
            if (char.IsUpper(msg[0]))
            {
                c = capitalization.capIni;
                if (char.IsUpper(msg[msg.Length - 1]))
                {
                    c = capitalization.allCaps;
                }
            }
            msg = msg.ToLower();//busca en minusculas y despues aplicaremos las mayusculas que sea necesario.

            foreach (tanslationsMsg item in trans.msg)
            {
                if (item.id == msg)
                {
                    foreach (tanslationsMsgTranslation m in item.Translation)
                    {
                        if (m.lang == Lang)
                        {
                            transMsg = m.Value;//Found
                            return SetCase(transMsg, c);
                        }
                    }
                }
            }
            return SetCase(msg, c); //NOT Found return the same
        }

        private string SetCase(string m, capitalization c)
        {
            CultureInfo ci = CultureInfo.InstalledUICulture;
            switch (c)
            {
                case capitalization.allLower:
                    m = m.ToLower();
                    break;
                case capitalization.capIni:
                    m = ci.TextInfo.ToTitleCase(m);
                    break;
                case capitalization.allCaps:
                    m = m.ToUpper();
                    break;
                default:
                    break;
            }
            return m;
        }
    }
}
