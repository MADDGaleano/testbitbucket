﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Security.Cryptography;


namespace DG_Utils
{
    class GenericosDG
    {
        /// <summary>
        /// Crea una folder con el atributo de hidden
        /// </summary>
        /// <param name="path"></param>
        /// <param name="visible"></param>
        /// <returns></returns>
        static public bool CreateDirectory(string path, bool visible)
        {
            try
            {
                // crea la carpeta
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    if (!visible)
                    {
                        di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                    }
                    System.Threading.Thread.Sleep(500); //wait for the operative system to create the folder;
                }
                return Directory.Exists(path);
            }
            catch (Exception)
            {
                return false;
            }
        }

        static public bool DeleteDirectory(string target_dir)
        {
            if (!Directory.Exists(target_dir))
            {
                return true;
            }

            try
            {
                string[] files = Directory.GetFiles(target_dir);
                string[] dirs = Directory.GetDirectories(target_dir);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }

                Directory.Delete(target_dir, true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static public bool IsFileLocked(string path)
        {
            FileInfo file = new FileInfo(path);

            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            // Application.DoEvents();
            System.Threading.Thread.Sleep(500);

            //file is not locked
            return false;
        }

        /// <summary>
        /// Serializa la clase y la guarda en un archivo XML
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        static public bool ObjToXmlFile(object Obj, string FileName, bool hideFile = false, int maxTries = 3, int sleepBetweenTries = 100)
        {
            int numtries = 0;

            if (File.Exists(FileName))
            {
                File.SetAttributes(FileName, ~FileAttributes.Hidden & ~FileAttributes.ReadOnly & FileAttributes.Normal);
                System.Threading.Thread.Sleep(500); // give the os chance to unhide file.
            }

            //while (true)//(!IsFileLocked(FileName) || !File.Exists(FileName))
            //{
            System.Threading.Thread.Sleep(sleepBetweenTries);
            //numtries++;
            try
            {
                var writer = new System.Xml.Serialization.XmlSerializer(Obj.GetType());
                var wfile = new System.IO.StreamWriter(FileName);
                writer.Serialize(wfile, Obj);
                wfile.Close();
                if (hideFile)   // Hacer que el archivo sea oculto o no
                    File.SetAttributes(FileName, FileAttributes.Hidden);
                return true;
            }
            catch (Exception e)
            {
                Add2DebugLog("ObjToXmlFile try#" + numtries.ToString() + " - " + e.Message.ToString());
                return false;
                //if (numtries >= maxTries)
                //{
                //    //Add2DebugLog("ObjToXmlFile try#" + numtries.ToString() + " - " + e.Message.ToString());
                //    return false;
                //}
            }
            //}
            //  return false;
        }

        /// <summary>
        /// Lee un archivo XML y lo retorna en un objeto serializable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FileName"></param>
        /// <returns></returns>
        static public T XmlFileToObj<T>(string FileName, int maxTries = 3, int sleepBetweenTries = 100)
        {
            int numtries = 0;
            while (true)
            {
                numtries++;
                try
                {
                    System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(T));
                    System.IO.StreamReader file = new System.IO.StreamReader(FileName);
                    object Obj = reader.Deserialize(file);
                    file.Close();
                    return (T)Obj;
                }
                catch (Exception e)
                {
                    Add2DebugLog("XmlFileToObj try#" + numtries.ToString() + " - " + e.Message.ToString());

                    if (numtries > maxTries)
                    {
                        Add2DebugLog("XmlFileToObj try#" + numtries.ToString() + " - " + e.Message.ToString());

                        return (T)Activator.CreateInstance(typeof(T));
                    }
                }
                System.Threading.Thread.Sleep(sleepBetweenTries);
            }
        }

        static public bool Add2DebugLog(string v)
        {// guarda en un archivo que debe quedar en AppData con el nombre del ejecutable.
            try
            {
                if (GenericosDG.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName, true))
                {
                    using (StreamWriter sw = File.AppendText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\ErrorLog" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".log"))
                    {
                        sw.WriteLine(string.Format("{0,16} {1}", DateTime.Now.ToLongTimeString(), v));
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        internal static void ViewTodaysErrorLog()
        {
            string logfile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\ErrorLog" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".log";
            try
            {
                Process.Start(logfile);
            }
            catch (Exception)
            {

            }
        }

        static public bool Add2SessionLog(string v)
        {// guarda en un archivo que debe quedar en AppData con el nombre del ejecutable.
            try
            {
                if (GenericosDG.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName, true))
                {
                    using (StreamWriter sw = File.AppendText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\Session" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".log"))
                    {
                        sw.WriteLine(string.Format("{0,16} {1}", DateTime.Now.ToLongTimeString(), v));
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal static void ViewTodaysSessionLog()
        {
            string logfile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\Session" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".log";
            try
            {
                Process.Start(logfile);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary> 
        /// This function encrypts the input text to an array of bytes. 
        /// </summary> 
        /// <param name="plainText">Human readable input text</param> 
        /// <param name="key">The symmetric key that is used for encryption</param> 
        /// <param name="IV">A random initialization vector</param> 
        /// <returns></returns> 
        public static byte[] EncryptStringToBytes_Aes(string plainText,
                                                    byte[] key, byte[] IV)
        {
            // Checking the arguments. 
            if (plainText.Length == 0)
                throw new ArgumentNullException("Source file size is zero.");
            if (key == null || key.Length == 0)
                throw new ArgumentNullException("Symmetric key is null.");
            if (IV == null || IV.Length == 0)
                throw new ArgumentNullException("Initilization Vector is null.");
            byte[] encrypted;
            // Creating an AesCryptoServiceProvider object with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = key;
                aesAlg.IV = IV;
                // Create a decrytor to perform the stream transform. 
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(
                                                aesAlg.Key, aesAlg.IV);
                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt,
                                            encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            // Write all data to the stream. 
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

        public static string GetUUID()
        {
            string value64 = string.Empty;
            string value32 = string.Empty;

           RegistryKey localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            localKey = localKey.OpenSubKey(@"SOFTWARE\Microsoft\Cryptography");
            if (localKey != null)
            {
                value64 = localKey.GetValue("MachineGuid", "").ToString();
            }

            if (value64 != string.Empty) return value64;

            RegistryKey localKey32 = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
            localKey32 = localKey32.OpenSubKey(@"SOFTWARE\Microsoft\Cryptography");
            if (localKey32 != null)
            {
                value32 = localKey32.GetValue("MachineGuid", "").ToString();
            }

            if (value32 != string.Empty) return value32;

            // If getting the windows id did not work lets get the HDD manufactures serial number (not volume serial)

            return GetHDDserial();
        }

        static private string GetHDDserial()
        {
            //we query the Win32_DiskDrive class:
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");

            //Now we need to extract the serial number from the Win32_PhysicalMedia class:

            foreach (ManagementObject wmi_HD in searcher.Get())
            {
                // get the hardware serial no.
                if (wmi_HD["SerialNumber"] != null)
                    return wmi_HD["SerialNumber"].ToString();
            }
            return "No HDD Serial";

        }

        public static string RenameFolder(string path, string newFolderName)
        {
            try
            {
                String newPath = Path.Combine(Path.GetDirectoryName(path), newFolderName);

                // rename to some temp name, to avoid locks
                Directory.Move(path, newPath + "__renameTemp__");
                Directory.Move(newPath + "__renameTemp__", newPath);

                return newPath;
            }
            catch (Exception)
            {

                return string.Empty;
            }
        }

        public static string WhereIsApplictionInstalled(string p_name)
        {
            string displayName;
            RegistryKey key;

            // search in: CurrentUser
            key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (String keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                displayName = subkey.GetValue("DisplayName") as string;
                if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return subkey.GetValue("InstallLocation").ToString();
                }
            }

            // search in: LocalMachine_32
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (String keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                displayName = subkey.GetValue("DisplayName") as string;
                if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return subkey.GetValue("InstallLocation").ToString();
                }
            }

            // search in: LocalMachine_64
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (String keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                displayName = subkey.GetValue("DisplayName") as string;
                if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return subkey.GetValue("InstallLocation").ToString();
                }
            }

            // NOT FOUND
            return String.Empty;
        }
    }
}
