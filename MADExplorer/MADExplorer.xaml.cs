﻿#pragma warning disable CA1307 // Specify StringComparison
#pragma warning disable CA1304 // Specify CultureInfo
#pragma warning disable CA1303 // Do not pass literals as localized parameters
#pragma warning disable CA1031 // Do not catch general exception types
#pragma warning disable CS0162 // Unreachable code detected
using GongSolutions.Shell;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Timer = System.Windows.Forms.Timer;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using DG_Utils;
using PreviewHost.Interop;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using Microsoft.Win32;
using System.Net.NetworkInformation;
using DG_Multilanguage;
using System.Threading.Tasks;
using System.Deployment.Application;
using System.Threading;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using Ookii.Dialogs.Wpf;

namespace MADExplorer
{
    public partial class MainWindow : Window, IPreviewHandlerManagedFrame
    {
        private List<string> SelectedLeft = new List<string>();
        private List<string> SelectedRight = new List<string>();
        private Timer aTimer = new Timer() { Interval = 2000, Enabled = false };
        private string uncLeft;
        private string uncRight;
        private Translate translations;
        private Machines machines = new Machines(); //aqui se mantiene la configuracion de maquinas global para usarla en el watch wilcom folder
        private bool starting = true; // if starting try to open CxD in hidden explorer.

        public string FilePv { get; set; }
        public static string SettingsFolder { get; set; } = AppData.MadIngDataFolder;

        // long time out for the ping becuase it is a refres
        private int pTimeOutL = Properties.Settings.Default.PingTimeoutL;
        // short time out for the ping to make it responsive
        private int pTimeOutC = 500;

        enum DriveMode { IP, Mapped, Both };
        DriveMode driveMode;
        public string EMBIntegrationFldr { get; set; } = Properties.Settings.Default.EMBIntegrationFldr;
        public bool isOperator { get; set; } = Properties.Settings.Default.isOperator;
        public bool useTSPro { get; set; } = Properties.Settings.Default.UseTSPro;

        public MainWindow()
        {
            GenericosDG.Add2SessionLog("Init");
            InitializeComponent();
            Do_Events();
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            mainWindow.Top = 0;
            mainWindow.Left = 0;
            // Set version on the window title
            try
            {
                string assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    assemblyVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4);
                }
                Title += ": " + assemblyVersion;
            }
            catch (Exception)
            {//ignore
            }

            _ = GenericosDG.Add2SessionLog("ApplyVisuals");
            ApplyVisuals();//activa y desactiva controles segun config

            _ = GenericosDG.Add2SessionLog("translations");
            //Load translations class
            translations = new Translate(SettingsFolder, Properties.Settings.Default.Language);
            TranslateInterfase(); // transalates all messages on the interfase

            driveMode = (DriveMode)Properties.Settings.Default.DriveMode;

            _ = GenericosDG.Add2SessionLog("AddDriveButtons");
            AddDriveButtons(); // adiciona los botones de las maquinas
            starting = false; // do not try to open drives in explorer anymore
            _ = GenericosDG.Add2SessionLog("LoadDrives");
            LoadDrives(); //cargar los listviews con los bordados


            ConfigMyWatcher(); // inicializa el timer

            // es necesario cargar el preview. Uso la primera imagen del panel de la izq.
            if (ListViewLeft.Items.Count > 0)
            {
                FileItem first = (FileItem)ListViewLeft.Items[0];
                _ = GenericosDG.Add2SessionLog("PreviewWindow");
                PreviewWindow(first.Filepath);
            }
            colPreview.MaxWidth = 0;

        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {

            Do_Events();
            //window maximized
            mainWindow.Left = 0;
            mainWindow.Top = 0;
            mainWindow.WindowState = WindowState.Maximized;

            CallingHome();
        }

        private void TranslateInterfase()
        {
            MnuFile.Header = translations.TranslateMsg("Archivo");
            BtnRefresh.ToolTip = translations.TranslateMsg("Refrescar");
            BtnDelete.ToolTip = translations.TranslateMsg("Borrar");
            BtnHelp.ToolTip = translations.TranslateMsg("Ayuda");
            BtnSettigns.ToolTip = translations.TranslateMsg("Configuración");
            chkMultiselect.Content = translations.TranslateMsg("Multiselección");
            CopyRight.ToolTip = translations.TranslateMsg("copiar hacia la derecha");
            CopyLeft.ToolTip = translations.TranslateMsg("copiar hacia la izquierda");
            MoveRight.ToolTip = translations.TranslateMsg("mover hacia la derecha");
            MoveLeft.ToolTip = translations.TranslateMsg("mover hacia la izquierda");
            AddRight.ToolTip = translations.TranslateMsg("adicionar archivo a la derecha");
            AddLeft.ToolTip = translations.TranslateMsg("adicionar archivo a la izquierda");
            toggleText.ToolTip = translations.TranslateMsg("vista previa");
            mnuConfigurar.Header = translations.TranslateMsg("Configuración");
            mnuLenguaje.Header = translations.TranslateMsg("Lenguaje");
        }

        private void ConfigMyWatcher()
        {
            _ = GenericosDG.Add2SessionLog("ConfigMyWatcher");
            // Create a timer with a 5 second interval.
            aTimer = new Timer
            {
                Interval = 5000,
                Enabled = true
            };
            aTimer.Tick += ATimer_Tick;
            aTimer.Start();

            StartWatchingWilcomFolders(); //Starts watching the wilcom folders
        }

        private bool DoneRefreshing = true;
        private void ATimer_Tick(object sender, EventArgs e)
        {
            if (DoneRefreshing)
            {
                DoneRefreshing = false;
                WatchForFilesLeft();
                Do_Events();
                WatchForFilesRight();
                Do_Events();
                if ((DateTime.Now - lastTimeWilcomWatcher).TotalSeconds > 180)
                { //if unexpected unhandled exeption occurs the watcher simply stops so every 3 min I'll check if its running fine.
                    StartWatchingWilcomFolders(); //Starts watching the wilcom folders
                    _ = GenericosDG.Add2SessionLog("Wilcom watcher was restarted");
                }
                if ((DateTime.Now - lastTimeRefreshDrives).TotalSeconds > 180)
                { //if unexpected unhandled exeption occurs the watcher simply stops so every 3 min I'll check if its running fine.
                    RefreshActiveDrives();
                }
                DoneRefreshing = true;
            }
        }

        private DateTime lastTimeRefreshDrives = DateTime.Now;
        private void RefreshActiveDrives()
        {
            return;
            try
            {
                if (PanelLeft.Children.Count >= 1)
                {
                    LinearGradientBrush gradientBrushOnLine = new LinearGradientBrush(Color.FromRgb(0, 200, 0), Color.FromRgb(255, 255, 255), new Point(0, 0), new Point(.2, .2));
                    LinearGradientBrush gradientBrushOffLine = new LinearGradientBrush(Color.FromRgb(255, 0, 0), Color.FromRgb(255, 255, 255), new Point(0, 0), new Point(.3, .3));

                    foreach (Button item in PanelLeft.Children)
                    {
                        string ip = item.ToolTip.ToString();
                        bool cxdIsAlive = CxDisAlive(ip, pTimeOutL);

                        if (!cxdIsAlive)
                        {
                            item.Background = gradientBrushOffLine;
                            item.IsEnabled = true;// no lo desactivo por si aparece despues
                            TextBlock t = (TextBlock)(item.Content);
                            t.Background = gradientBrushOffLine;
                        }
                        else
                        {
                            item.Background = gradientBrushOnLine;
                            item.IsEnabled = true;
                            TextBlock t = (TextBlock)(item.Content);
                            t.Background = gradientBrushOnLine;
                        }
                        Do_Events(); // paint left before reading right
                    }
                    Do_Events(); // paint left before reading right
                    foreach (Button item in PanelRight.Children)
                    {
                        string ip = item.ToolTip.ToString();
                        bool cxdIsAlive = CxDisAlive(ip, pTimeOutL);

                        if (!cxdIsAlive)
                        {
                            item.Background = gradientBrushOffLine;
                            item.IsEnabled = true; // no lo desactivo por si aparece despues
                            TextBlock t = (TextBlock)(item.Content);
                            t.Background = gradientBrushOffLine;
                        }
                        else
                        {
                            item.Background = gradientBrushOnLine;
                            item.IsEnabled = true;
                            TextBlock t = (TextBlock)(item.Content);
                            t.Background = gradientBrushOnLine;
                        }
                        Do_Events();
                    }
                    Do_Events();
                }
            }
            catch (Exception)
            {
                //ignore
            }
            lastTimeRefreshDrives = DateTime.Now;
        }

        private void LoadDrives()
        {
            // Restaura el último estado de drives seleccionados
            // o carga el primero a la iz y el ultimo a la der si los de la 
            // sesion anterior no estan disponibles
            if (PanelLeft.Children.Count >= 1)
            {
                bool leftok = false;
                bool rightok = false;

                foreach (Button item in PanelLeft.Children)
                {
                    if (item.Name == Properties.Settings.Default.LeftDrive)
                    {
                        FillListView(item);
                        leftok = true;
                        break;
                    }
                }
                Do_Events(); // paint left before reading right
                foreach (Button item in PanelRight.Children)
                {
                    if (item.Name == Properties.Settings.Default.RightDrive)
                    {
                        FillListView(item);
                        rightok = true;
                        break;
                    }
                }

                // si los de la última sesión no están disponibles entonces primero y ultimo
                if (!leftok)
                    FillListView(PanelLeft.Children[0] as Button);
                if (!rightok)
                    FillListView(PanelRight.Children[PanelRight.Children.Count - 1] as Button);
            }
        }

        private void FillListView(Button b)
        {

            aTimer.Enabled = false; //stop timer

            string name = b.Name;
            string tag = b.Tag.ToString();
            string txt = ((TextBlock)b.Content).Text;
            string ip = (string)b.ToolTip;
            string unc = @"\\" + ip + @"\davwwwroot\";
            if (ip == null)
            {
                unc = name + @":\\";
            }

            //clear list view and selected files
            if (tag == "Left")
            {
                ListViewLeft.Items.Clear();
                SelectedLeft.Clear();
                prevFilesLeft.Clear();
                Properties.Settings.Default.LeftDrive = name;
            }
            else
            {
                ListViewRight.Items.Clear();
                SelectedRight.Clear();
                prevFilesRight.Clear();
                Properties.Settings.Default.RightDrive = name;
            }

            // Add files on directory to the list view
            DirectoryInfo dirinfo = new DirectoryInfo(unc);

            // Para que no se pegue si no hay cxd disponibles
            if (!CxDisAlive(ip, pTimeOutC)) return;

            try
            {
                if (Directory.Exists(dirinfo.FullName))
                {
                    IOrderedEnumerable<FileInfo> files = dirinfo.GetFiles().OrderBy(f => f.Name);
                    foreach (var file in files)
                    {
                        if (file.Name != "Thumbs.db")
                        {
                            AddFileItem(file, tag);
                        }
                    }

                    //set tab names and add files to prevlist
                    if (tag == "Left")
                    {
                        TabItemLeft.Header = txt;
                        uncLeft = unc;
                        foreach (var f in files)
                        {
                            prevFilesLeft.Add(f.ToString());
                        }
                    }
                    else
                    {
                        TabItemRight.Header = txt;
                        uncRight = unc;
                        foreach (var f in files)
                        {
                            prevFilesRight.Add(f.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ = GenericosDG.Add2DebugLog($"FillListView/dirinfo.GetFiles:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
            }

            try
            {
                aTimer.Enabled = true; //restarts timer
            }
            catch (Exception ex)
            {
                _ = GenericosDG.Add2DebugLog($"FillListView/aTimer.Enable:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
            }
        }

        private void AddFileItem(FileInfo file, string tag)
        {
            string fileName = file.Name;
            string fullName = file.FullName;
            var item = new FileItem
            {
                Width = 120,
                Height = 140,
                Padding = new Thickness(1),
                Margin = new Thickness(1),
                Background = Brushes.Transparent,
                BorderBrush = Brushes.Transparent,
                Tag = tag,
                ToolTip = fileName,
                Name = GenericosDG.CleanButtonName(fileName),
            };
            //TODO Cuadrar los eventos. Asi no es posible el drag and drop.
            item.MouseDoubleClick += Item_MouseDoubleClick;
            // item.PreviewMouseLeftButtonDown += Item_PreviewMouseLeftButtonDown;
            item.MouseRightButtonDown += Item_MouseRightButtonDown; ;
            item.Click += Item_Click;
            item.FillItem(fullName, fileName);

            if (tag == "Left")
            {
                if (ListViewLeft.Items.Count > 0)
                {
                    string lviN = item.Name;
                    for (int j = 0; j < ListViewLeft.Items.Count; j++)
                    {
                        FileItem lvj = (FileItem)ListViewLeft.Items[j];
                        string lvjN = lvj.Name;
                        if (string.Compare(lviN, lvjN) < 0)
                        {
                            ListViewLeft.Items.Insert(j, item);
                            break;
                        }
                        if (j == ListViewLeft.Items.Count - 1)
                        {
                            ListViewLeft.Items.Add(item);
                            break;
                        }
                    }
                }
                else
                {
                    ListViewLeft.Items.Add(item);
                }
            }
            else
            {
                if (ListViewRight.Items.Count > 0)
                {
                    string lviN = item.Name;
                    for (int j = 0; j < ListViewRight.Items.Count; j++)
                    {
                        FileItem lvj = (FileItem)ListViewRight.Items[j];
                        string lvjN = lvj.Name;
                        if (string.Compare(lviN, lvjN) < 0)
                        {
                            ListViewRight.Items.Insert(j, item);
                            break;
                        }
                        if (j == ListViewRight.Items.Count - 1)
                        {
                            ListViewRight.Items.Add(item);
                            break;
                        }
                    }
                }
                else
                {
                    ListViewRight.Items.Add(item);
                }
            }
        }

        private void Item_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            FileItem fileItem = sender as FileItem;
            // fileItem.Background = new SolidColorBrush(Colors.Red);
            DataObject dataObj = new DataObject(fileItem);
            DragDrop.DoDragDrop(fileItem, dataObj, DragDropEffects.Copy);
        }

        private void Item_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {// abrir con TrueSizer
            string pathToTrueSizer = WhereInstalled(Properties.Settings.Default.WilcomTSName);
            if (!string.IsNullOrEmpty(pathToTrueSizer))
            { // abrir EMB si existe o si no entonces abrir el DST (o el archivo que recibió el dobleclick)
                string fileClicked = ((FileItem)sender).Filepath;
                string targetPath = Regex.Replace(fileClicked, "davwwwroot", @"davwwwroot\emb", RegexOptions.IgnoreCase);
                string targetExt = Path.GetExtension(fileClicked);
                string targetFile = Regex.Replace(targetPath, targetExt, ".EMB", RegexOptions.IgnoreCase);
                if (File.Exists(targetFile))
                {
                    fileClicked = targetFile;
                }

                using (Process pr = new Process())
                {
                    pr.StartInfo.FileName = pathToTrueSizer + Properties.Settings.Default.WilcomTSexe;
                    pr.StartInfo.Arguments = fileClicked;
                    pr.StartInfo.ErrorDialog = true; // importante para dejar que salga el error del sisema operativo
                    pr.Start();
                }
            }
            else
            {// si TrueSizer no esta instalado abrir con el OS.
                GenericosDG.OpenWithTheOS(((FileItem)sender).Filepath);
            }
        }

        /// <summary>
        /// Finds the insallation path of any installed program
        /// It uses the uninstall sections on the registry for that.
        /// </summary>
        /// <param name="name">Name of the exe file</param>
        /// <returns></returns>
        private static string WhereInstalled(string name)
        {
            string displayName;
            RegistryKey key;
            key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (string keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                displayName = subkey.GetValue("DisplayName") as string;
                if (name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return subkey.GetValue("InstallLocation").ToString();
                }
            }

            // search in: LocalMachine_32
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (string keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                displayName = subkey.GetValue("DisplayName") as string;
                if (name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return subkey.GetValue("InstallLocation").ToString();
                }
            }

            // search in: LocalMachine_64
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (string keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                displayName = subkey.GetValue("DisplayName") as string;
                if (name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return subkey.GetValue("InstallLocation").ToString();
                }
            }

            // NOT FOUND
            return string.Empty;
        }

        private void Item_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {// todo mostrar menu contextual
         //Dispatcher.BeginInvoke(DispatcherPriority.Normal, openConext(sender));

            if (false)
            {
                //El siguiente codigo muestra el menu contextual del S.O. (no sirve)

                FileItem item = (FileItem)sender;
                ShellItem myFile = new ShellItem(new Uri(item.Filepath));
                ShellContextMenu ctxmenu = new ShellContextMenu(myFile);
                ctxmenu.ShowContextMenu(null, HelpersContextMenu.GetMousePosition());
            }
        }

        //private Delegate openConext(object sender)
        //{
        //    ContextMenu cm;
        //    cm = this.FindResource("mContext") as ContextMenu;
        //    cm.PlacementTarget = sender as Button;
        //    cm.StaysOpen = true;
        //    cm.IsOpen = true;
        //}


        /// <summary>
        /// Al hacer click en los items los va adicionando a una lista
        /// para luego operar con ellos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_Click(object sender, RoutedEventArgs e)
        {
            FileItem item = (FileItem)sender;

            UnloadPreview();
            PreviewWindow(item.Filepath);
            if (colPreview.ActualWidth > 0)
            {
                LoadPreview();
            }

            // si no hay multiselección se elimina lo seleccionado y se adiciona 1
            if (!chkMultiselect.IsChecked ?? true)
            {
                ClearSelection();
            }

            switch (item.Tag)
            {
                case "Left":
                    //add item to the selected if not already, else remove
                    if (!SelectedLeft.Contains(item.Filepath))
                    {
                        SelectedLeft.Add(item.Filepath);
                        item.Background = new SolidColorBrush(Color.FromRgb(0x1D, 0x99, 0xCC));
                    }
                    else
                    {
                        SelectedLeft.Remove(item.Filepath);
                        item.Background = Brushes.Transparent;
                    }
                    break;
                case "Right":
                    //add item to the selected if not already, else remove
                    if (!SelectedRight.Contains(item.Filepath))
                    {
                        SelectedRight.Add(item.Filepath);
                        item.Background = new SolidColorBrush(Color.FromRgb(0x1D, 0x99, 0xCC));
                    }
                    else
                    {
                        SelectedRight.Remove(item.Filepath);
                        item.Background = Brushes.Transparent;
                    }
                    break;
                default:
                    break;
            }
        }

        private void AddDriveButtons()
        {
            //Clear existing buttons
            PanelLeft.Children.Clear();
            PanelRight.Children.Clear();

            if (driveMode == DriveMode.IP || driveMode == DriveMode.Both)
            {// open IP defined drives
                string machinesCfgFile = SettingsFolder + @"Machines.xml";
                if (File.Exists(machinesCfgFile))
                {
                    machines = GenericosDG.XmlFileToObj<Machines>(machinesCfgFile);
                }
                else
                {// create empty machinesCfgFile and open config window
                    Machines _m = new Machines
                    {
                        Machine = new MachinesMachine[]
                        {
                            new MachinesMachine {Name = "",IP = "",Active = true}
                        },
                    };
                    //guarda y lee el config nuevo
                    GenericosDG.ObjToXmlFile(_m, machinesCfgFile);
                    machines = GenericosDG.XmlFileToObj<Machines>(machinesCfgFile);
                    OpenConfigWindow();
                }

                if (machines.Machine != null && !string.IsNullOrEmpty(machines.Machine[0].IP))
                {
                    foreach (MachinesMachine m in machines.Machine)
                    {
                        if (m.Active)
                        {
                            AddDriveButtonCfg(m.IP, m.Name);
                            Do_Events();
                        }
                    }
                }
                else
                {
                    OpenConfigWindow();
                }
            }
            else if (driveMode == DriveMode.Mapped || driveMode == DriveMode.Both)
            {//Get Mapped Drives
                foreach (var di in DriveInfo.GetDrives())
                {
                    //Add button
                    AddDriveButton(di);
                }
            }
        }

        private void AddDriveButton(DriveInfo di)
        {
            if (di.IsReady && DriveOperations.CheckDriveIsCXD(di, out string machineName))
            {
                var nameLeft = new TextBlock
                {
                    Text = di.Name.Substring(0, 1) + ": " + machineName,
                    FontSize = 20,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness { Left = 5, Right = 5 },
                    Name = "Path"
                };

                Button bl = new Button
                {
                    Height = 30,
                    Content = nameLeft,
                    Name = di.Name.Substring(0, 1),
                    Tag = "Left",
                };
                bl.Click += B_Click;
                bl.HorizontalAlignment = HorizontalAlignment.Left;
                //Grid.SetColumn(b, 1);
                PanelLeft.Children.Add(bl);

                // now the other button for the right pannel
                var nameRight = new TextBlock
                {
                    Text = di.Name.Substring(0, 1) + ": " + machineName,
                    FontSize = 20,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness { Left = 5, Right = 5 },
                    Name = "Path"
                };
                Button br = new Button
                {
                    Height = bl.Height,
                    Content = nameRight,
                    Name = di.Name.Substring(0, 1),
                    Tag = "Right",
                };
                br.Click += B_Click;
                br.HorizontalAlignment = HorizontalAlignment.Left;
                PanelRight.Children.Add(br);
            }
        }

        private void AddDriveButtonCfg(string ip, string name)
        {
            name = GenericosDG.CleanButtonName(name);

            bool cxdIsAlive = CxDisAlive(ip, pTimeOutL);
            // add drives active if alive or inactive if off-line 
            {
                LinearGradientBrush gradientBrushOnLine = new LinearGradientBrush(Color.FromRgb(0, 200, 0), Color.FromRgb(255, 255, 255), new Point(0, 0), new Point(.2, .2));
                LinearGradientBrush gradientBrushOffLine = new LinearGradientBrush(Color.FromRgb(255, 0, 0), Color.FromRgb(255, 255, 255), new Point(0, 0), new Point(.3, .3));
                //gradientBrushOffLine.StartPoint.Offset(0, 1);

                TextBlock nameLeft = new TextBlock
                {
                    Text = name,
                    FontSize = 20,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness { Left = 5, Right = 5 },
                    Name = "Path"
                };

                Button bl = new Button
                {
                    Height = 30,
                    Content = nameLeft,
                    Name = name,
                    Tag = "Left",
                    ToolTip = ip,
                    IsEnabled = true, //antes =cxdIsAlive, pero ya no lo desactivo por si aparece despues
                };
                bl.Click += B_Click;
                bl.HorizontalAlignment = HorizontalAlignment.Left;

                if (!cxdIsAlive)
                {
                    nameLeft.Background = gradientBrushOffLine;
                    bl.Background = gradientBrushOffLine;
                }
                else
                {
                    nameLeft.Background = gradientBrushOnLine;
                    bl.Background = gradientBrushOnLine;
                }
                //Grid.SetColumn(b, 1);
                PanelLeft.Children.Add(bl);

                // now the other button for the right pannel
                var nameRight = new TextBlock
                {
                    Text = name,
                    FontSize = 20,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness { Left = 5, Right = 5 },
                    Name = "Path"
                };
                Button br = new Button
                {
                    Height = bl.Height,
                    Content = nameRight,
                    Name = name,
                    Tag = "Right",
                    ToolTip = ip,
                    IsEnabled = true, //antes =cxdIsAlive, pero ya no lo desactivo por si aparece despues
                };
                br.Click += B_Click;
                br.HorizontalAlignment = HorizontalAlignment.Left;

                if (!cxdIsAlive)
                {
                    nameRight.Background = gradientBrushOffLine;
                    br.Background = gradientBrushOffLine;
                }
                else
                {
                    nameRight.Background = gradientBrushOnLine;
                    br.Background = gradientBrushOnLine;
                }

                PanelRight.Children.Add(br);
            }

            // algunas veces es necesario navegar en el explorardor al CxD para que Windows
            // lo vea entonces si da ping pero windows no lo ve, vamos a intentar
            // navegar lo mas secretamente posible a esa ruta para que Windows lo detecte.
            if (starting && cxdIsAlive) // dió ping pero no sabemos si va a responder o no.
            {
                Task.Run(() => OpenCxDInExplorer(ip));
            }
        }

        private static void OpenCxDInExplorer(string ip)
        {
            try
            {
                string site = @"\\" + ip + @"\davwwwroot\";
                //      string strOutput = $"{site} - Application: {NativeMethods.FindExecutable(site)}";
                Process myProcess = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "explorer.exe",//"" + site + "",
                        WindowStyle = ProcessWindowStyle.Minimized,
                        UseShellExecute = true,
                        Arguments = site,
                    }
                };

                myProcess.Start();
                Thread.Sleep(5000);
                myProcess.Close();
                myProcess.Dispose();
            }
            catch (Exception ex)
            {
                _ = GenericosDG.Add2DebugLog($"OpenCxDInExplorer/:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
            }
            CloseExplorerWindows();
        }

        private static void CloseExplorerWindows()

        {
            SHDocVw.ShellWindows shellWindows = new SHDocVw.ShellWindows();

            string filename;

            List<SHDocVw.InternetExplorer> toClose = new List<SHDocVw.InternetExplorer>();
            foreach (SHDocVw.InternetExplorer ie in shellWindows)
            {
                filename = Path.GetFileNameWithoutExtension(ie.FullName).ToLower();

                if (filename.Equals("explorer"))
                {
                    // Save the location off to your application
                    Console.WriteLine("Explorer location : {0}", ie.LocationURL);

                    if (ie.LocationURL.Contains("davwwwroot"))
                    {
                        toClose.Add(ie);
                    }
                    // Setup a trigger for when the user navigates
                    //  ie.NavigateComplete2 += new SHDocVw.DWebBrowserEvents2_NavigateComplete2EventHandler(handlerMethod);
                }
            }

            foreach (var item in toClose)
            {
                item.Quit();
            }


            /// Este bloque cierra TODOS los exploerers por eso es mejor el anterior
            /// que solo cierra los que sean davwwwwroot
            //Shell32.Shell shell = new Shell32.Shell();

            //// ref: Shell.Windows method
            //// https://msdn.microsoft.com/en-us/library/windows/desktop/bb774107(v=vs.85).aspx
            //System.Collections.IEnumerable windows = shell.Windows() as System.Collections.IEnumerable;
            //if (windows != null)
            //{
            //    // ref: ShellWindows object
            //    // https://msdn.microsoft.com/en-us/library/windows/desktop/bb773974(v=vs.85).aspx
            //    foreach (SHDocVw.InternetExplorer window in windows)
            //    {
            //        object doc = window.Document;
            //        if (doc != null && doc is Shell32.ShellFolderView)
            //        {
            //            window.Quit();  // closes the window
            //        }
            //    }
            //}


        }

        private static bool CxDisAlive(string ip, int pTimeOut)
        {//hace ping y ver si esta vivo.
            try
            {
                using (Ping p = new Ping())
                {
                    PingReply reply = p.Send(ip, pTimeOut);
                    if (reply.Status == IPStatus.Success)
                    {
                        string mac = GenericosDG.GetMacAddress(ip);
                        if (Properties.Settings.Default.Mac.Contains(mac.Substring(0, 8)))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        private static string CxDAliveMac(string ip)
        {//hace ping y ver si esta vivo.
            try
            {
                using (Ping p = new Ping())
                {
                    PingReply reply = p.Send(ip, 500);
                    if (reply.Status == IPStatus.Success)
                    {
                        string mac = GenericosDG.GetMacAddress(ip);
                        if (Properties.Settings.Default.Mac.Contains(mac.Substring(0, 8)))
                        {
                            //Task x = GenericosDG.FileWriteAsync(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\Mac" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".log", mac);
                            return mac;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return "";
        }

        private void AddImageButton()
        {
            Button b = new Button
            {
                Width = 30,
                Height = 30,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri(
                        @"pack://application:,,,/"
                        + Assembly.GetExecutingAssembly().GetName().Name
                        + ";component/"
                        + "/Images/add.png", UriKind.RelativeOrAbsolute)),
                    VerticalAlignment = VerticalAlignment.Center
                },
                Name = "Z",
                Tag = "Z:\\",
            };
            b.Click += B_Click;
            Grid.SetColumn(b, 1);
            MainGrid.Children.Add(b);
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            LinearGradientBrush gradientBrushOnLine = new LinearGradientBrush(Color.FromRgb(0, 200, 0), Color.FromRgb(255, 255, 255), new Point(0, 0), new Point(.2, .2));
            LinearGradientBrush gradientBrushOffLine = new LinearGradientBrush(Color.FromRgb(255, 0, 0), Color.FromRgb(255, 255, 255), new Point(0, 0), new Point(.3, .3));

            var clickedButton = ((Button)sender);
            string ip = clickedButton.ToolTip.ToString();
            bool cxdIsAlive = CxDisAlive(ip, pTimeOutL);

            if (!cxdIsAlive)
            {
                clickedButton.Background = gradientBrushOffLine;
                clickedButton.IsEnabled = true;
                TextBlock t = (TextBlock)(clickedButton.Content);
                t.Background = gradientBrushOffLine;
            }
            else
            {
                clickedButton.Background = gradientBrushOnLine;
                clickedButton.IsEnabled = true;
                TextBlock t = (TextBlock)(clickedButton.Content);
                t.Background = gradientBrushOnLine;
            }
            Do_Events(); // paint left before reading right
            FillListView(sender as Button);
        }

        private void BtnInfo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.IsWilcomVersion)
                GenericosDG.BrowserTo(@"Help\ExplorerWHelp-" + translations.Lang + ".html");
            else
                GenericosDG.BrowserTo(@"Help\ExplorerMHelp-" + translations.Lang + ".html");
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            DoDelete();
        }

        private void DoDelete()
        {
            string msg1 = translations.TranslateMsg("Desea borrar") + " ";
            string msg2 = translations.TranslateMsg("Borrar");
            string m = translations.TranslateMsg("no se pudo eliminar");
            const string Caption = "Error: ";

            if (SelectedLeft.Count > 0)
            {
                if (CxDisAlive(SelectedLeft[0].Split('\\')[2], pTimeOutL))
                {
                    foreach (string fileName in SelectedLeft)
                    {
                        if (File.Exists(fileName) && MessageBox.Show(msg1 + Path.GetFileName(fileName), msg2, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        {
                            try
                            {
                                File.Delete(fileName);

                                //Delete the BMP if exists
                                string targetPathBmp = Regex.Replace(fileName, "davwwwroot", @"davwwwroot\bmp", RegexOptions.IgnoreCase);
                                string ext = Path.GetExtension(fileName);
                                string destFileBmp = Regex.Replace(targetPathBmp, ext, ".BMP", RegexOptions.IgnoreCase);
                                File.Delete(destFileBmp);

                                //Delete the BMP if exists
                                string targetPathEMB = Regex.Replace(fileName, "davwwwroot", @"davwwwroot\emb", RegexOptions.IgnoreCase);
                                string destFileEMB = Regex.Replace(targetPathEMB, ext, ".EMB", RegexOptions.IgnoreCase);
                                File.Delete(destFileEMB);

                                FileInfo fi = new FileInfo(fileName);
                                RemoveFileItem(Path.GetFileName(fileName), "Left");
                            }
                            catch (Exception ex)
                            {
                                _ = MessageBox.Show(m + " " + fileName + " " + TabItemLeft.Header.ToString() + " - \n\r\n\r" + ex.ToString(),
                                                    caption: Caption,
                                                    button: MessageBoxButton.OK,
                                                    icon: MessageBoxImage.Error);
                            }
                        }
                    }
                }
                else
                {
                    string maq = TabItemLeft.Header.ToString();
                    MessageBox.Show(translations.TranslateMsg("máquina no disponible") + ": " + maq,
                            translations.TranslateMsg("Intente más tardE"), MessageBoxButton.OK);
                }
            }
            if (SelectedRight.Count > 0)
            {
                if (CxDisAlive(SelectedRight[0].Split('\\')[2], pTimeOutL))
                {
                    foreach (string fileName in SelectedRight)
                    {
                        if (File.Exists(fileName) && MessageBox.Show(msg1 + Path.GetFileName(fileName), msg2, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        {
                            try
                            {
                                File.Delete(fileName);

                                //Delete the BMP if exists
                                string targetPathBmp = Regex.Replace(fileName, "davwwwroot", @"davwwwroot\bmp", RegexOptions.IgnoreCase);
                                string ext = Path.GetExtension(fileName);
                                string destFileBmp = Regex.Replace(targetPathBmp, ext, ".BMP", RegexOptions.IgnoreCase);
                                File.Delete(destFileBmp);

                                //Delete the EMB if exists
                                string targetPathEMB = Regex.Replace(fileName, "davwwwroot", @"davwwwroot\emb", RegexOptions.IgnoreCase);
                                string destFileEMB = Regex.Replace(targetPathEMB, ext, ".EMB", RegexOptions.IgnoreCase);
                                File.Delete(destFileEMB);

                                FileInfo fi = new FileInfo(fileName);
                                RemoveFileItem(Path.GetFileName(fileName), "Right");
                            }
                            catch (Exception ex)
                            {
                                _ = MessageBox.Show(m + " " + fileName + " " + TabItemRight.Header.ToString() + " - \n\r\n\r" + ex.ToString(),
                                                    caption: Caption,
                                                    button: MessageBoxButton.OK,
                                                    icon: MessageBoxImage.Error);
                            }
                        }
                    }
                }
                else
                {
                    string maq = TabItemRight.Header.ToString();
                    MessageBox.Show(translations.TranslateMsg("máquina no disponible") + ": " + maq,
                            translations.TranslateMsg("Intente más tardE"), MessageBoxButton.OK);
                }
            }
            //ClearSelection
            ClearSelection();
        }

        private void ClearSelection(string side = "all")
        {
            if (side == "Right" || side == "all")
            {
                SelectedRight.Clear();
                foreach (FileItem item in ListViewRight.Items)
                {
                    item.Background = Brushes.Transparent;
                }
            }

            if (side == "Left" || side == "all")
            {
                SelectedLeft.Clear();
                foreach (FileItem item in ListViewLeft.Items)
                {
                    item.Background = Brushes.Transparent;
                }
            }
        }

        private void BtnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshDrives();
        }

        private void RefreshDrives()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            //add drive buttons
            AddDriveButtons();

            //load list views
            LoadDrives();
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void CopyLeft_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            string sourcePath = uncRight;
            string targetPath = uncLeft;
            CopyMoveEMBandBMPFiles(sourcePath, targetPath, SelectedRight, false);
            //ClearSelection
            ClearSelection("Right");
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void CopyRight_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            string sourcePath = uncLeft;
            string targetPath = uncRight;
            CopyMoveEMBandBMPFiles(sourcePath, targetPath, SelectedLeft, false);
            //ClearSelection
            ClearSelection("Left");
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void MoveLeft_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            string sourcePath = uncRight;
            string targetPath = uncLeft;
            CopyMoveEMBandBMPFiles(sourcePath, targetPath, SelectedRight, true);
            //ClearSelection
            ClearSelection("Right");
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void MoveRight_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            string sourcePath = uncLeft;
            string targetPath = uncRight;
            CopyMoveEMBandBMPFiles(sourcePath, targetPath, SelectedLeft, true);
            //ClearSelection
            ClearSelection("Left");
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void CopyMoveEMBandBMPFiles(string sourcePath, string targetPath, List<string> selectedFiles, bool move, bool overwrite = false)
        {
            foreach (string f in selectedFiles)
            {// Use Path class to manipulate file and directory paths.
                string fileName = Path.GetFileName(f);
                string sourceFile = Path.Combine(sourcePath, fileName);
                string destFile = Path.Combine(targetPath, fileName);
                string sourcePathBmp = Regex.Replace(sourcePath, "davwwwroot", @"davwwwroot\bmp", RegexOptions.IgnoreCase);
                string sourcePathEMB = Regex.Replace(sourcePath, "davwwwroot", @"davwwwroot\emb", RegexOptions.IgnoreCase);
                string targetPathBmp = Regex.Replace(targetPath, "davwwwroot", @"davwwwroot\bmp", RegexOptions.IgnoreCase);
                string targetPathEMB = Regex.Replace(targetPath, "davwwwroot", @"davwwwroot\emb", RegexOptions.IgnoreCase);
                string ext = Path.GetExtension(fileName);
                string fileNameBmp = Regex.Replace(fileName, ext, ".BMP", RegexOptions.IgnoreCase);
                string sourceFileBmp = Path.Combine(sourcePathBmp, fileNameBmp);
                string destFileBmp = Path.Combine(targetPathBmp, fileNameBmp);
                string fileNameEMB = Regex.Replace(fileName, ext, ".EMB", RegexOptions.IgnoreCase);
                string sourceFileEMB = Path.Combine(sourcePathEMB, fileNameEMB);
                string destFileEMB = Path.Combine(targetPathEMB, fileNameEMB);

                try
                {
                    if (CxDisAlive(targetPath.Split('\\')[2], pTimeOutL))
                    {
                        if (!Directory.Exists(targetPath))
                        {
                            Directory.CreateDirectory(targetPath);
                        }
                        if (!Directory.Exists(targetPathBmp))
                        {
                            Directory.CreateDirectory(targetPathBmp);
                        }
                        if (!Directory.Exists(targetPathEMB))
                        {
                            Directory.CreateDirectory(targetPathEMB);
                        }
                    }
                    else
                    {
                        MessageBox.Show(translations.TranslateMsg("folder destino no disponible"),
                            translations.TranslateMsg("error en el destino"), MessageBoxButton.OK);
                        return;// nothing to copy since destination is not available
                    }
                }
                catch (Exception)
                {// could not create destination folder
                    // lo dejo que sigua para que el siguiente bloque guarde el mensaje de excepcion
                }
                if (!CxDisAlive(sourcePath.Split('\\')[2], pTimeOutL) || !File.Exists(sourceFile))
                {// No deberia ser pero si el origen no existe, no hace nada
                    MessageBox.Show(translations.TranslateMsg("folder origen no disponible"),
                          translations.TranslateMsg("error en el origen"), MessageBoxButton.OK);
                    return;
                }
                string yaexiste = translations.TranslateMsg("Ya existe") + " ";
                string wanttooverwrite = " " + translations.TranslateMsg("Desea sobrescribirlo");
                string _overwrite = translations.TranslateMsg("Sobrescribir");
                if (!File.Exists(destFile) || overwrite || MessageBox.Show(yaexiste + fileName + wanttooverwrite, _overwrite, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (File.Exists(destFile))
                        {
                            File.Delete(destFile);
                        }
                        if (File.Exists(destFileBmp))
                        {
                            File.Delete(destFileBmp);
                        }
                        if (File.Exists(destFileEMB))
                        {
                            File.Delete(destFileEMB);
                        }
                        if (move)
                        {
                            File.Move(sourceFile, destFile);
                            if (File.Exists(sourceFileBmp))
                            {
                                File.Move(sourceFileBmp, destFileBmp);
                            }
                            if (File.Exists(sourceFileEMB))
                            {
                                File.Move(sourceFileEMB, destFileEMB);
                            }
                        }
                        else
                        {
                            File.Copy(sourceFile, destFile, true);
                            if (File.Exists(sourceFileBmp))
                            {
                                File.Copy(sourceFileBmp, destFileBmp, true);
                            }
                            if (File.Exists(sourceFileEMB))
                            {
                                File.Copy(sourceFileEMB, destFileEMB, true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _ = GenericosDG.Add2DebugLog($"CopymoveEMBandBMPfiles/:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                    }
                }
            }
        }

        private void CopyMoveFiles(string sourcePath, string targetPath, List<string> selectedFiles, bool move, bool overwrite = false)
        {
            foreach (string f in selectedFiles)
            {// Use Path class to manipulate file and directory paths.
                string fileName = Path.GetFileName(f);
                string sourceFile = Path.Combine(sourcePath, fileName);
                string destFile = Path.Combine(targetPath, fileName);

                try
                {
                    if (!Directory.Exists(targetPath))
                    {
                        Directory.CreateDirectory(targetPath);
                    }
                }
                catch (Exception)
                {// could not create destination folder
                    // lo dejo que sigua para que el siguiente bloque guarde el mensaje de excepcion
                }
                if (!File.Exists(sourceFile))
                {// No deberia ser pero si el origen no existe, no hace nada
                    return;
                }
                string yaexiste = translations.TranslateMsg("Ya existe") + " ";
                string wanttooverwrite = " " + translations.TranslateMsg("Desea sobrescribirlo");
                string _overwrite = translations.TranslateMsg("Sobrescribir");
                if (!File.Exists(destFile) || overwrite || MessageBox.Show(yaexiste + fileName + wanttooverwrite, _overwrite, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (File.Exists(destFile))
                        {
                            File.Delete(destFile);
                        }
                        if (move)
                        {
                            File.Move(sourceFile, destFile);
                        }
                        else
                        {
                            File.Copy(sourceFile, destFile, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        _ = GenericosDG.Add2DebugLog($"CopyMoveFiles/:  Error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                    }
                }
            }
        }

        private void RemoveFileItem(string fi, string side)
        {
            if (side == "Right")
            {
                for (int i = 0; i < ListViewRight.Items.Count; i++)
                {
                    FileItem item = (FileItem)ListViewRight.Items[i];
                    if (item.ToolTip.ToString() == fi)
                    {
                        ListViewRight.Items.Remove(item);
                    }
                }
            }
            else
            {
                for (int i = 0; i < ListViewLeft.Items.Count; i++)
                {
                    FileItem item = (FileItem)ListViewLeft.Items[i];
                    if (item.ToolTip.ToString() == fi)
                    {
                        ListViewLeft.Items.Remove(item);
                    }
                }
            }
        }

        private void ListViewLeft_Drop(object sender, DragEventArgs e)
        {

        }

        private void ListViewRight_Drop(object sender, DragEventArgs e)
        {
            FileItem fileItem = (FileItem)e.Data.GetData(typeof(FileItem));

            StackPanel stp = fileItem.Content as StackPanel;
            TextBlock txb = stp.Children[1] as TextBlock;
            MessageBox.Show(txb.ToolTip.ToString());
        }

        List<string> prevFilesLeft = new List<string>();
        private void WatchForFilesLeft()
        {
            const string direction = "Left";
            string unc = uncLeft;

            try
            {
                List<string> files = new List<string>();

                // si el directorio existe y el cxd esta online busca los archivos
                if (CxDisAlive(unc.Split('\\')[2], pTimeOutC) && Directory.Exists(unc))
                {// Collect file list
                    files = Directory.GetFiles(unc).ToList();
                    //OfflineLeft.Visibility = Visibility.Hidden;

                    // Then look for differences against the previous list
                    IEnumerable<string> added = files.Except(prevFilesLeft);
                    if (added.Any())
                    {
                        foreach (string path in added)
                        {
                            FileInfo fi = new FileInfo(path);
                            AddFileItem(fi, direction);
                        }
                        //sortListView(direction);
                    }

                    IEnumerable<string> removed = prevFilesLeft.Except(files);
                    if (removed.Any())
                    {
                        foreach (string path in removed)
                        {
                            RemoveFileItem(Path.GetFileName(path), direction);
                        }
                    }
                    prevFilesLeft = files;
                }
                else
                {
                    // OfflineLeft.Visibility = Visibility.Visible;
                }
            }
            catch (Exception)
            {
                // the drive did not respond
                //remove everything
                IEnumerable<string> removed = prevFilesLeft;
                if (removed != null && removed.Any())
                {
                    foreach (string path in removed)
                    {
                        RemoveFileItem(Path.GetFileName(path), direction);
                    }
                }
                if (prevFilesLeft != null)
                {
                    prevFilesLeft.Clear(); // the new list is empty
                }
            }
        }

        List<string> prevFilesRight = new List<string>();
        private void WatchForFilesRight()
        {
            const string direction = "Right";
            string unc = uncRight;

            try
            {
                List<string> files = new List<string>();

                // si el directorio existe y el cxd esta online busca los archivos
                if (CxDisAlive(unc.Split('\\')[2], pTimeOutC) && Directory.Exists(unc))
                {// Collect file list
                    files = Directory.GetFiles(unc).ToList();
                    //OfflineRight.Visibility = Visibility.Hidden;
                    // Then look for differences against the previous list
                    IEnumerable<string> added = files.Except(prevFilesRight);
                    if (added.Any())
                    {
                        foreach (string path in added)
                        {
                            FileInfo fi = new FileInfo(path);
                            AddFileItem(fi, direction);
                        }
                        //sortListView(direction);
                    }

                    IEnumerable<string> removed = prevFilesRight.Except(files);
                    if (removed.Any())
                    {
                        foreach (string path in removed)
                        {
                            RemoveFileItem(Path.GetFileName(path), direction);
                        }
                    }
                    prevFilesRight = files;
                }
                else
                {
                    // OfflineRight.Visibility = Visibility.Visible;
                }
            }
            catch (Exception)
            {
                // the drive did not respond
                //remove everything
                IEnumerable<string> removed = prevFilesRight;
                if (removed != null && removed.Any())
                {
                    foreach (string path in removed)
                    {
                        RemoveFileItem(Path.GetFileName(path), direction);
                    }
                }
                if (prevFilesRight != null)
                {
                    prevFilesRight.Clear(); // the new list is empty
                }
            }
        }

        private void Configurar_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                Properties.Settings.Default.IsWilcomVersion = !Properties.Settings.Default.IsWilcomVersion;
                ApplyVisuals();
            }
            OpenConfigWindow();
        }

        private void ApplyVisuals()
        {
            try
            {
                if (!Properties.Settings.Default.IsWilcomVersion)
                {
                    AddLeft.Visibility = Visibility.Visible;
                    AddRight.Visibility = Visibility.Visible;
                    BtnPwrByWilcom.Visibility = Visibility.Collapsed;
                }
                else
                {
                    AddLeft.Visibility = Visibility.Hidden;
                    AddRight.Visibility = Visibility.Hidden;
                    BtnPwrByWilcom.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                _ = GenericosDG.Add2DebugLog($"ApplyVisuals/:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
            }
        }

        private void OpenConfigWindow()
        {
            ConfigMachines c = new ConfigMachines
            {
                EMBIntegration = EMBIntegrationFldr,
                isOperatorFlg = isOperator,
                useTSProFlag = useTSPro                
            };

            try
            {
                c.Owner = this;
            }
            catch (Exception)
            {
                //ignore
            }
            finally
            {
                aTimer.Enabled = false; // disable watching
                StopWatchingWilcomFolders(); // stops watching Wilcom folders
                _ = c.ShowDialog();
                if (c.Changes)
                {
                    EMBIntegrationFldr = c.EMBIntegration;
                    Properties.Settings.Default.EMBIntegrationFldr = EMBIntegrationFldr;
                    Properties.Settings.Default.isOperator = c.isOperatorFlg;
                    Properties.Settings.Default.UseTSPro = c.useTSProFlag;
                    isOperator = c.isOperatorFlg;
                    useTSPro = c.useTSProFlag;
                    //reload
                    RefreshDrives();
                }
                StartWatchingWilcomFolders(); // restarts watching Wilcom folders
                aTimer.Enabled = true; // re-enable watching
            }
        }

        #region PreviewHandler
        WindowInteropHelper interop;
        PreviewHandler currentHandler;
        public IntPtr WindowHandle => interop.Handle;

        public PreviewHost.Interop.Rect PreviewerBounds
        {
            get
            {
                var source = PresentationSource.FromVisual(contentPresenter);
                var transformToDevice = source.CompositionTarget.TransformToDevice;
                var transformToWindow = contentPresenter.TransformToAncestor(this);
                var physicalSize = (Size)transformToDevice.Transform((Vector)contentPresenter.RenderSize);
                var physicalPos = transformToDevice.Transform(transformToWindow.Transform(new Point(0, 0)));
                PreviewHost.Interop.Rect result = new PreviewHost.Interop.Rect
                {
                    Left = (int)(physicalPos.X + 0.5),
                    Top = (int)(physicalPos.Y + 0.5),
                    Right = (int)(physicalPos.X + physicalSize.Width + 0.5),
                    Bottom = (int)(physicalPos.Y + physicalSize.Height + 0.5)
                };
                return result;
            }
        }

        // We will be interested in Tab, Ctrl+B, F5 and Esc.
        public IEnumerable<AcceleratorEntry> GetAcceleratorTable()
        {
            yield return new AcceleratorEntry { IsVirtual = 1, Key = 0x09 }; // Tab
            yield return new AcceleratorEntry { IsVirtual = 1, Key = 0x42 }; // B
            yield return new AcceleratorEntry { IsVirtual = 1, Key = 0x74 }; // F5
            yield return new AcceleratorEntry { IsVirtual = 1, Key = 0x1b }; // Esc
        }
        public bool TranslateAccelerator(ref MSG msg)
        {
            if (msg.message != 0x100)
                return false;
            var vk = msg.wParam.ToInt64();
            ModifierKeys modState = ModifierKeys.None;
            Dispatcher.Invoke(() => modState = Keyboard.Modifiers);
            Action action = null;
            try
            {
                switch (vk)
                {
                    case 0x09:
                        if ((modState & ModifierKeys.Shift) == ModifierKeys.Shift)
                        {
                            action = () => contentPresenter.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                        }
                        else
                        {
                            action = () => contentPresenter.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        }
                        return true;
                    case 0x42:
                        if ((modState & (ModifierKeys.Control | ModifierKeys.Alt | ModifierKeys.Shift)) == ModifierKeys.Control)
                        {
                            action = () =>
                            {
                                //BrowseCommand.Execute(null);
                                contentPresenter.Focus();
                            };
                            return true;
                        }
                        return false;
                    case 0x74:
                        if ((modState & (ModifierKeys.Control | ModifierKeys.Alt | ModifierKeys.Shift | ModifierKeys.Windows)) == ModifierKeys.None)
                        {
                            action = () =>
                            {
                                // RefreshCommand.Execute(null);
                                contentPresenter.Focus();
                            };
                            return true;
                        }
                        return false;
                    case 0x1b:
                        if ((modState & (ModifierKeys.Control | ModifierKeys.Alt | ModifierKeys.Shift | ModifierKeys.Windows)) == ModifierKeys.None)
                        {
                            // action = () => ExitCommand.Execute(null);
                            return true;
                        }
                        return false;
                    default:
                        return false;
                }
            }
            finally
            {
                if (action != null)
                    Dispatcher.InvokeAsync(action, DispatcherPriority.Send);
            }
        }

        public static readonly DependencyProperty PreviewStatusTextProperty = DependencyProperty.Register("PreviewStatusText", typeof(string), typeof(MainWindow));
        public string PreviewStatusText
        {
            get { return (string)GetValue(PreviewStatusTextProperty); }
            set { SetValue(PreviewStatusTextProperty, value); }
        }


        private void PreviewContent_GotFocus(object sender, RoutedEventArgs e)
        {
            if (currentHandler != null)
            {
                currentHandler.Focus();
                if (currentHandler.QueryFocus() == IntPtr.Zero)
                {
                    var old = currentHandler;
                    currentHandler = null;
                    contentPresenter.Focus();
                    currentHandler = old;
                }
            }
        }
        public void LoadPreview()
        {
            togglePreview.Visibility = Visibility.Visible;

            if (currentHandler != null)
            {
                return;
            }

            var clsid = PreviewHandlerDiscovery.FindPreviewHandlerFor(Path.GetExtension(FilePv), interop.Handle);
            if (clsid == null)
            {
                PreviewStatusText = "No preview handler is associated with this file type.";
                return;
            }
            //IntPtr pobj = IntPtr.Zero;
            try
            {
                currentHandler = new PreviewHandler(clsid.Value, this);
                currentHandler.InitWithFileWithEveryWay(FilePv);
            }
            catch (Exception ex)
            {
                togglePreview.Visibility = Visibility.Hidden;
                UnloadPreview();
                PreviewStatusText = "Preview handler is malfunctioning:\r\n" + ex.Message;
                return;
            }
            _ = currentHandler.SetBackground(((SolidColorBrush)Background).Color);
            _ = currentHandler.SetForeground(((SolidColorBrush)Foreground).Color);
            currentHandler.DoPreview();
            // In case the handler crashes, this text is revealed to the user.
            // A real-world application might try restarting the handler before
            // giving up. Also, a read-world application should NOT put this
            // text visible to Narrator before the preview handler crashes.
            // For now, we just keep it simple.
            PreviewStatusText = "A preview should be here.";
        }

        public void UnloadPreview()
        {
            PreviewStatusText = "Preview is not loaded.";
            if (currentHandler == null)
                return;
            currentHandler.UnloadPreview();
            currentHandler = null;
        }


        private void PreviewHost_Loaded(object sender, RoutedEventArgs e)
        {
            if (FilePv != null)
            {
                LoadPreview();
            }
        }
        public void PreviewWindow(string file)
        {
            try
            {
                FilePv = file;
                interop = null;
                interop = new WindowInteropHelper(this);
                interop.EnsureHandle();
            }
            catch (Exception ex)
            {
                _ = GenericosDG.Add2DebugLog($"PreviewWindow/CXD vacio:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
            }
        }

        private void PreviewHost_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (currentHandler != null)
            {
                _ = currentHandler.ResetBounds();
            }
        }

        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            if (wtoken != null)
            {
                wtoken.Cancel();
            }
            try
            {
                taskWatchWilcomFldr.Dispose();
            }
            catch
            { }
            if (aTimer != null)
                aTimer.Enabled = false; //stop timer

            //force the end of the app
            App.Current.Shutdown();
            Process.GetCurrentProcess().Kill();
        }

        private void TogglePreview_Click(object sender, RoutedEventArgs e)
        {
            int _from, _to;
            if (colPreview.ActualWidth == 0)
            {
                _from = 0;
                _to = 300;
                toggleText.Text = "<";
            }
            else
            {
                _from = 300;
                _to = 0;
                toggleText.Text = ">";
            }

            Storyboard storyboard = new Storyboard();

            Duration duration = new Duration(TimeSpan.FromMilliseconds(2000));
            CubicEase ease = new CubicEase { EasingMode = EasingMode.EaseOut };

            DoubleAnimation animation = new DoubleAnimation
            {
                EasingFunction = ease,
                Duration = duration,
                From = _from,
                To = _to
            };
            storyboard.Children.Add(animation);
            Storyboard.SetTarget(animation, colPreview);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(ColumnDefinition.MaxWidth)"));

            storyboard.Begin();
        }

        private void AddLeft_Click(object sender, RoutedEventArgs e) => AddFiles(uncLeft);

        private void AddRight_Click(object sender, RoutedEventArgs e) => AddFiles(uncRight);
        private void AddFiles(string uncDest)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = Properties.Settings.Default.ValidExt,
                FilterIndex = 7,
            };
            if (openFileDialog.ShowDialog() == true)
            {
                File.Copy(openFileDialog.FileName, uncDest + Path.GetFileName(openFileDialog.FileName), true);
            }
        }

        private void BtnPwrByWilcom_Click(object sender, RoutedEventArgs e)
        {
            GenericosDG.BrowserTo(Properties.Settings.Default.wilcomurl);
        }

        private void BtnPwrByMad_Click(object sender, RoutedEventArgs e)
        {
            GenericosDG.BrowserTo(Properties.Settings.Default.madingenierosurl);
        }

        private void Lng_es_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Language = "es";
            translations.Lang = "es";
            TranslateInterfase();
        }

        private void Lng_en_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Language = "en";
            translations.Lang = "en";
            TranslateInterfase();
        }

        //destID: destination ID
        //text: text to send
        private async Task CallHome(string destID)
        {
            if (!Debugger.IsAttached) // only send telegram if not in VS IDE
            {
                string text = "Starting EMB: " + Title;
                string localIP = "";
                string userName = "";
                string domain = "";
                ///// LOCAL IP /////
                try
                {
                    localIP = GenericosDG.GetLocalIPAddress();
                    userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    domain = IPGlobalProperties.GetIPGlobalProperties().DomainName;
                }
                catch (Exception ex)
                {
                    _ = GenericosDG.Add2DebugLog($"SendingTg/GetLocalIP:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                }

                ///// REMOTE IP /////
                string remoteIP = GenericosDG.GetPublicIp();

                ///// CONFIG FILE /////
                string machinesCfgFile = SettingsFolder + @"Machines.xml";
                if (File.Exists(machinesCfgFile))
                {
                    machines = GenericosDG.XmlFileToObj<Machines>(machinesCfgFile);
                }

                XmlSerializer xsSubmit = new XmlSerializer(typeof(Machines));
                string machinesCfg;
                using (StringWriter sww = new StringWriter())
                {
                    using (System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(sww))
                    {
                        xsSubmit.Serialize(writer, machines);
                        machinesCfg = sww.ToString();
                    }
                }

                ///// MAC ADDRESS DE LOS FA CONECTADOS /////
                string lstMac = "";
                if (machines.Machine != null)
                {
                    foreach (MachinesMachine m in machines.Machine)
                    {
                        if (m.Active)
                        {
                            lstMac += CxDAliveMac(m.IP) + "\n";
                        }
                    }
                }

                ///// WILCOM APPLICATIONS /////
                string wilcom = SearchWilcomProducts();

                ///// SEND /////
                try
                {
                    text += "\n" + Environment.MachineName + "\\" + domain
                          + "\n" + localIP + " - usr:" + userName
                          + "\n" + remoteIP
                          + "\n"
                          + "\n" + machinesCfg
                          + "\n"
                          + "\n" + lstMac
                          + "\n" + wilcom
                    //+ "\n" + pathToWilcom == "" ? "No Wilcom" : "Wilcom: " + pathToWilcom
                    //+ "\n" + pathToTrueSizer == "" ? "No TrueSizer" : "TrueSizer: " + pathToTrueSizer
                    //+ "\n" + pathToDeco == "" ? "No Deco" : "Deco: " + pathToDeco
                    //+ "\n" + pathToWSExt == "" ? "No ShellExt" : "ShellExt: " + pathToWSExt
                    ;
                    var bot = new Telegram.Bot.TelegramBotClient(Properties.Settings.Default.tgFather);

                    _ = await bot.SendTextMessageAsync(destID, text).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    _ = GenericosDG.Add2DebugLog($"Sending Tg msg:  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                }
            }
        }

        private static void Do_Events()
        {
            try
            {
                _ = Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));
            }
            catch (Exception)
            {
                //ignore
            }
        }

        private void CallingHome()
        {
            try
            { Task x = CallHome(Properties.Settings.Default.tgChann); }
            catch (Exception)
            { //ignore
            }
        }

        #region WatchingWilcomFolders
        private CancellationTokenSource wtoken;
        private Task taskWatchWilcomFldr;

        void StopWatchingWilcomFolders()
        {
            if (wtoken == null)
            {
                return;
            }
            wtoken.Cancel();
            try
            {
                taskWatchWilcomFldr.Wait();
            }
            catch (AggregateException) { }
        }

        private DateTime lastTimeWilcomWatcher = DateTime.Now;
        void StartWatchingWilcomFolders()
        {
            wtoken = new CancellationTokenSource();

            taskWatchWilcomFldr = Task.Run(async () =>  // <- marked async
            {
                while (true)
                {
                    DoWatchingWilcomFolders();
                    await Task.Delay(10000, wtoken.Token).ConfigureAwait(false); // <- await with cancellation
                }
            }, wtoken.Token);
        }

        void DoWatchingWilcomFolders()
        {
            lastTimeWilcomWatcher = DateTime.Now; // the last time the watcher executed

            foreach (MachinesMachine m in machines.Machine)
            {
                string target = @"\\" + m.IP + @"\davwwwroot\";

                if (Directory.Exists(m.WFolder))
                {
                    //first check for DGT files and convert them to BMP 24 bits
                    DirectoryInfo di = new DirectoryInfo(m.WFolder);
                    List<string> files = Directory.GetFiles(m.WFolder, "*.DGT").ToList();
                    foreach (string f in files)
                    {
                        System.Drawing.Bitmap bm8bit = new System.Drawing.Bitmap(f);
                        System.Drawing.Bitmap bm24bit = new System.Drawing.Bitmap(bm8bit.Width, bm8bit.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bm24bit);
                        g.DrawImage(bm8bit, 0, 0, bm8bit.Width, bm8bit.Height);
                        bm24bit.Save(Regex.Replace(f, ".DGT", ".BMP", RegexOptions.IgnoreCase), System.Drawing.Imaging.ImageFormat.Bmp);
                        g.Dispose();
                        bm8bit.Dispose();
                        bm24bit.Dispose();
                        try
                        {
                            File.Delete(f);
                        }
                        catch (Exception)
                        {
                            //ignore;
                        }
                    }
                    Do_Events();
                    // move the DGT (now BMP) to the SD_WLAN\BMP folder
                    files = Directory.GetFiles(m.WFolder).Where(name => name.ToLower().EndsWith(".bmp")).ToList();
                    CopyMoveFiles(m.WFolder, target + @"BMP\", files, true, true);

                    // move the EMB to the SD_WLAN\EMB folder
                    files = Directory.GetFiles(m.WFolder).Where(name => name.ToLower().EndsWith(".emb")).ToList();
                    CopyMoveFiles(m.WFolder, target + @"EMB\", files, true, true);

                    // For each DST searches for EMB if EMBIntegration is enabled
                    if (!string.IsNullOrEmpty(EMBIntegrationFldr))
                    {
                        files = Directory.GetFiles(m.WFolder).Where(name => name.ToLower().EndsWith(".dst")).ToList();
                        Task.Run(() => GetEMBFilesForIntegration(files, target)); ;
                        CopyMoveFiles(m.WFolder, target, files, true, true);
                    }

                    // now move everything else to the root folder
                    files = Directory.GetFiles(m.WFolder).Where(name => !name.ToLower().EndsWith(".dgt")).ToList();
                    CopyMoveFiles(m.WFolder, target, files, true, true);
                }
            }
        }

        private void GetEMBFilesForIntegration(List<string> files, string target)
        {
            foreach (string f in files)
            {
                try
                {
                    string embName = Path.GetFileNameWithoutExtension(f) + ".emb";
                    var embFile = Directory.GetFiles(EMBIntegrationFldr, embName, SearchOption.AllDirectories);
                    if (embFile.ToList<string>().Count > 0)
                    {
                        CopyMoveFiles(Path.GetDirectoryName(embFile[0]), target + @"\EMB", embFile.ToList<string>(), false, true);
                    }

                }
                catch (Exception ex)
                {
                    _ = GenericosDG.Add2DebugLog($"GetEMBFilesForIntegration/  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                }
            }
        }

        #endregion

        private static string SearchWilcomProducts()
        {
            string displayName;
            string answer = "";
            RegistryKey key;
            key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
            try
            {
                answer += "CurrentUser\n";
                foreach (string keyName in key.GetSubKeyNames())
                {
                    RegistryKey subkey = key.OpenSubKey(keyName);
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains("Wilcom"))
                    {
                        answer += displayName + " : " + subkey.GetValue("InstallLocation").ToString() + "\n";
                    }
                }
            }
            catch (Exception)
            {
                //ignore
            }

            // search in: LocalMachine_32
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            try
            {
                answer += "LocalMachine32\n";
                foreach (string keyName in key.GetSubKeyNames())
                {
                    RegistryKey subkey = key.OpenSubKey(keyName);
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains("Wilcom"))
                    {
                        answer += displayName + " : " + subkey.GetValue("InstallLocation").ToString() + "\n";
                    }
                }
            }
            catch (Exception)
            {
                //ignore
            }

            // search in: LocalMachine_64
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
            try
            {
                answer += "LocalMachine64\n";
                foreach (string keyName in key.GetSubKeyNames())
                {
                    RegistryKey subkey = key.OpenSubKey(keyName);
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains("Wilcom"))
                    {
                        answer += displayName + " : " + subkey.GetValue("InstallLocation").ToString() + "\n";
                    }
                }
            }
            catch (Exception)
            {
                //ignore
            }
            return answer;
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            if (sizeInfo == null) return;

            base.OnRenderSizeChanged(sizeInfo);

            //Calculate half of the offset to move the form

            if (sizeInfo.HeightChanged)
            {
                Top += (sizeInfo.PreviousSize.Height - sizeInfo.NewSize.Height) / 2;
            }

            if (sizeInfo.WidthChanged)
            {
                Left += (sizeInfo.PreviousSize.Width - sizeInfo.NewSize.Width) / 2;
            }
        }

        private void BtnOpenTS_Click(object sender, RoutedEventArgs e)
        {
            List<string> selectedAll = new List<string>();
            selectedAll.AddRange(SelectedLeft);
            selectedAll.AddRange(SelectedRight);
            Task.Run(() => BtnOpenTSAsync(selectedAll));
        }

        private void BtnOpenTSAsync(List<string> selectedAll)
        {
            if (selectedAll.Count >= 1)
            {
                // abrir con TrueSizer
                string pathToTrueSizer = WhereInstalled(Properties.Settings.Default.WilcomTSName);
                if (!string.IsNullOrEmpty(pathToTrueSizer))
                { // abrir EMB si existe o si no entonces abrir el DST (o el archivo que recibió el dobleclick)
                    foreach (string fileClicked in selectedAll)
                    {
                        string fileToOpen = fileClicked;
                        string targetEMBPath = Regex.Replace(fileClicked, "davwwwroot", @"davwwwroot\emb", RegexOptions.IgnoreCase);
                        string fileClickedExt = Path.GetExtension(fileClicked);
                        string targetEMBFile = Regex.Replace(targetEMBPath, fileClickedExt, ".EMB", RegexOptions.IgnoreCase);
                        if (File.Exists(targetEMBFile))
                        {
                            fileToOpen = targetEMBFile;
                        }

                        using (Process pr = new Process())
                        {
                            pr.StartInfo.FileName = pathToTrueSizer + Properties.Settings.Default.WilcomTSexe;
                            pr.StartInfo.Arguments = fileToOpen;
                            pr.StartInfo.ErrorDialog = true; // importante para dejar que salga el error del sisema operativo
                            pr.Start();

                            while (!pr.HasExited)
                            {
                                Thread.Sleep(1000);
                            }
                        }
                    }
                }
            }
        }

        private void ClearDrive_Click(object sender, RoutedEventArgs e)
        {
            string machine;
            string sourcePath;
            bool confirmation = false;

            if (((Button)sender).Name == "ClearLeft")
            {
                machine = TabItemLeft.Header.ToString();
                sourcePath = uncLeft;
            }
            else
            {
                machine = TabItemRight.Header.ToString();
                sourcePath = uncRight;
            }

            if (TaskDialog.OSSupportsTaskDialogs)
            {
                using (TaskDialog d = new TaskDialog())
                {
                    d.WindowTitle = translations.TranslateMsg("Borrar todo");
                    d.MainInstruction = translations.TranslateMsg("ESTA SEGURO DE QUE QUIERE BORRAR TODOS LOS ARCHIVOS");
                    d.Content = String.Join(string.Empty, "BORRAR TODO", " ", machine);
                    d.ExpandedInformation = translations.TranslateMsg("esta operación borrará todos los archivos");
                    d.Footer = translations.TranslateMsg("Esta operación es definitiva y no se pude deshacer");
                    d.FooterIcon = TaskDialogIcon.Warning;
                    d.EnableHyperlinks = true;
                    //TaskDialogButton customButton = new TaskDialogButton("A custom button");
                    TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);
                    TaskDialogButton cancelButton = new TaskDialogButton(ButtonType.Cancel);
                    //d.Buttons.Add(customButton);
                    d.Buttons.Add(okButton);
                    d.Buttons.Add(cancelButton);
                    //d.HyperlinkClicked += new EventHandler<HyperlinkClickedEventArgs>(TaskDialog_HyperLinkClicked);
                    TaskDialogButton b = d.ShowDialog(this);
                    if (b == okButton)
                    {
                        confirmation = true;
                    }
                    //    MessageBox.Show(this, "You clicked the custom button", "Task Dialog Sample");
                    //else if (b == okButton)
                    //    MessageBox.Show(this, "You clicked the OK button.", "Task Dialog Sample");
                }
            }
            else
            {
                if (MessageBox.Show(this, translations.TranslateMsg("ESTA SEGURO DE QUE QUIERE BORRAR TODOS LOS ARCHIVOS"), translations.TranslateMsg("folder duplicado"), MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    confirmation = true;
                }
            }

            if (confirmation)
            {
                DeleteAllFilesin(sourcePath);
            }
        }

        private void DeleteAllFilesin(string sourcePath)
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;

                foreach (string f in Directory.EnumerateFiles(sourcePath, "*.*"))
                {
                    File.Delete(f);
                }
                if (Directory.Exists(sourcePath + @"BMP\"))
                {
                    foreach (string f in Directory.EnumerateFiles(sourcePath + @"BMP\"))
                    {
                        File.Delete(f);
                    }
                }
                if (Directory.Exists(sourcePath + @"EMB\"))
                {
                    foreach (string f in Directory.EnumerateFiles(sourcePath + @"EMB\"))
                    {
                        File.Delete(f);
                    }
                }
            }
            catch (Exception)
            {
                //ignore
            }
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.None:
                    break;
                case Key.Cancel:
                    break;
                case Key.Back:
                    break;
                case Key.Tab:
                    break;
                case Key.LineFeed:
                    break;
                case Key.Clear:
                    break;
                case Key.Enter:
                    break;
                case Key.Pause:
                    break;
                case Key.CapsLock:
                    break;
                case Key.Escape:
                    break;
                case Key.ImeConvert:
                    break;
                case Key.ImeNonConvert:
                    break;
                case Key.ImeAccept:
                    break;
                case Key.ImeModeChange:
                    break;
                case Key.Space:
                    break;
                case Key.PageUp:
                    break;
                case Key.PageDown:
                    break;
                case Key.End:
                    break;
                case Key.Home:
                    break;
                case Key.Left:
                    break;
                case Key.Up:
                    break;
                case Key.Right:
                    break;
                case Key.Down:
                    break;
                case Key.Select:
                    break;
                case Key.Print:
                    break;
                case Key.Execute:
                    break;
                case Key.PrintScreen:
                    break;
                case Key.Insert:
                    break;
                case Key.Delete:
                    DoDelete();
                    break;
                case Key.Help:
                    break;
                case Key.D0:
                    break;
                case Key.D1:
                    break;
                case Key.D2:
                    break;
                case Key.D3:
                    break;
                case Key.D4:
                    break;
                case Key.D5:
                    break;
                case Key.D6:
                    break;
                case Key.D7:
                    break;
                case Key.D8:
                    break;
                case Key.D9:
                    break;
                case Key.A:
                    break;
                case Key.B:
                    break;
                case Key.C:
                    break;
                case Key.D:
                    break;
                case Key.E:
                    break;
                case Key.F:
                    break;
                case Key.G:
                    break;
                case Key.H:
                    break;
                case Key.I:
                    break;
                case Key.J:
                    break;
                case Key.K:
                    break;
                case Key.L:
                    break;
                case Key.M:
                    break;
                case Key.N:
                    break;
                case Key.O:
                    break;
                case Key.P:
                    break;
                case Key.Q:
                    break;
                case Key.R:
                    break;
                case Key.S:
                    break;
                case Key.T:
                    break;
                case Key.U:
                    break;
                case Key.V:
                    break;
                case Key.W:
                    break;
                case Key.X:
                    break;
                case Key.Y:
                    break;
                case Key.Z:
                    break;
                case Key.LWin:
                    break;
                case Key.RWin:
                    break;
                case Key.Apps:
                    break;
                case Key.Sleep:
                    break;
                case Key.NumPad0:
                    break;
                case Key.NumPad1:
                    break;
                case Key.NumPad2:
                    break;
                case Key.NumPad3:
                    break;
                case Key.NumPad4:
                    break;
                case Key.NumPad5:
                    break;
                case Key.NumPad6:
                    break;
                case Key.NumPad7:
                    break;
                case Key.NumPad8:
                    break;
                case Key.NumPad9:
                    break;
                case Key.Multiply:
                    break;
                case Key.Add:
                    break;
                case Key.Separator:
                    break;
                case Key.Subtract:
                    break;
                case Key.Decimal:
                    break;
                case Key.Divide:
                    break;
                case Key.F1:
                    break;
                case Key.F2:
                    break;
                case Key.F3:
                    break;
                case Key.F4:
                    break;
                case Key.F5:
                    break;
                case Key.F6:
                    break;
                case Key.F7:
                    break;
                case Key.F8:
                    break;
                case Key.F9:
                    break;
                case Key.F10:
                    break;
                case Key.F11:
                    break;
                case Key.F12:
                    break;
                case Key.F13:
                    break;
                case Key.F14:
                    break;
                case Key.F15:
                    break;
                case Key.F16:
                    break;
                case Key.F17:
                    break;
                case Key.F18:
                    break;
                case Key.F19:
                    break;
                case Key.F20:
                    break;
                case Key.F21:
                    break;
                case Key.F22:
                    break;
                case Key.F23:
                    break;
                case Key.F24:
                    break;
                case Key.NumLock:
                    break;
                case Key.Scroll:
                    break;
                case Key.LeftShift:
                    break;
                case Key.RightShift:
                    break;
                case Key.LeftCtrl:
                    break;
                case Key.RightCtrl:
                    break;
                case Key.LeftAlt:
                    break;
                case Key.RightAlt:
                    break;
                case Key.BrowserBack:
                    break;
                case Key.BrowserForward:
                    break;
                case Key.BrowserRefresh:
                    break;
                case Key.BrowserStop:
                    break;
                case Key.BrowserSearch:
                    break;
                case Key.BrowserFavorites:
                    break;
                case Key.BrowserHome:
                    break;
                case Key.VolumeMute:
                    break;
                case Key.VolumeDown:
                    break;
                case Key.VolumeUp:
                    break;
                case Key.MediaNextTrack:
                    break;
                case Key.MediaPreviousTrack:
                    break;
                case Key.MediaStop:
                    break;
                case Key.MediaPlayPause:
                    break;
                case Key.LaunchMail:
                    break;
                case Key.SelectMedia:
                    break;
                case Key.LaunchApplication1:
                    break;
                case Key.LaunchApplication2:
                    break;
                case Key.OemSemicolon:
                    break;
                case Key.OemPlus:
                    break;
                case Key.OemComma:
                    break;
                case Key.OemMinus:
                    break;
                case Key.OemPeriod:
                    break;
                case Key.OemQuestion:
                    break;
                case Key.OemTilde:
                    break;
                case Key.AbntC1:
                    break;
                case Key.AbntC2:
                    break;
                case Key.OemOpenBrackets:
                    break;
                case Key.OemPipe:
                    break;
                case Key.OemCloseBrackets:
                    break;
                case Key.OemQuotes:
                    break;
                case Key.Oem8:
                    break;
                case Key.OemBackslash:
                    break;
                case Key.ImeProcessed:
                    break;
                case Key.System:
                    break;
                case Key.DbeAlphanumeric:
                    break;
                case Key.OemFinish:
                    break;
                case Key.OemCopy:
                    break;
                case Key.OemAuto:
                    break;
                case Key.OemEnlw:
                    break;
                case Key.OemBackTab:
                    break;
                case Key.Attn:
                    break;
                case Key.CrSel:
                    break;
                case Key.ExSel:
                    break;
                case Key.EraseEof:
                    break;
                case Key.Play:
                    break;
                case Key.Zoom:
                    break;
                case Key.NoName:
                    break;
                case Key.DbeEnterDialogConversionMode:
                    break;
                case Key.OemClear:
                    break;
                case Key.DeadCharProcessed:
                    break;
                default:
                    break;
            }
        }


    }
}
