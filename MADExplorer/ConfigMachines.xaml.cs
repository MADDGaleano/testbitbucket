﻿using DG_Multilanguage;
using DG_Utils;
using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace MADExplorer
{
    /// <summary>
    /// Interaction logic for ConfigMachines.xaml
    /// </summary>
    public partial class ConfigMachines : Window
    {
        private Translate translations;

        public bool Changes { get; set; } = false;
        private static string SettingsFolder { get; set; } = AppData.MadIngDataFolder;
        public string EMBIntegration { get; set; }
        public bool isOperatorFlg { get; set; }
        public bool useTSProFlag { get; set; }

        public ConfigMachines()
        {
            InitializeComponent();
            Mouse.OverrideCursor = Cursors.Arrow;
            //Load translations class
            translations = new Translate(SettingsFolder, Properties.Settings.Default.Language);
            TranslateInterfase();
        }

        private void TranslateInterfase()
        {
            cpConfigInfo.InfoToShow = translations.TranslateMsg("cpInfo");
            cpConfigInfo.Header = translations.TranslateMsg("Instrucciones");
            cfgHeaderText.Text = translations.TranslateMsg("Configuración de EMB Explorer");
            addMachine.ToolTip = translations.TranslateMsg("Adicionar máquina");
            lblStartIP.Content = translations.TranslateMsg("IP Inicio");
            lblEndIp.Content = translations.TranslateMsg("IP Fin");
            btnScan.Content = translations.TranslateMsg("Buscar máquinas");
            btnCancel.Content = translations.TranslateMsg("Cancelar");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string machinesCfgFile = SettingsFolder + @"Machines.xml";
            Machines machines = GenericosDG.XmlFileToObj<Machines>(machinesCfgFile);
            ShowConnfiguredMachines(machines);
            EMBIntegrationFldr.Text = EMBIntegration;
            chkTruesizerpro.IsChecked = useTSProFlag;
            chkOperator.IsChecked = isOperatorFlg;
        }

        private void ShowConnfiguredMachines(Machines _machines)
        {
            if (_machines.Machine != null)
            {
                foreach (MachinesMachine m in _machines.Machine)
                {
                    AddMachine(m);
                }
            }
        }

        private enum ColOrder { machName = 0, cpyName = 1, machIp = 2, wilcomFolder = 3, openWilcomFldr = 4, cpyWilcomFolder = 5, machAct = 6 }
        private enum ColWidth { machName = 25, cpyName = 5, machIp = 25, wilcomFolder = 25, openWilcomFldr = 5, cpyWilcomFolder = 5, machAct = 5 }

        private void AddMachine(MachinesMachine m)
        {
            int gridWidth = 600;
            int linesHeight = 19;

            if (LBox.Items.Count == 0) //la lista esta vacia. Add title
            {
                Grid gt = new Grid
                {
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    Height = linesHeight * 2,
                    Width = gridWidth,
                    Margin = new Thickness(0),
                    VerticalAlignment = VerticalAlignment.Top,
                    ShowGridLines = true,
                };

                // machine name
                ColumnDefinition c1t = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.machName, GridUnitType.Star) };
                // Button to copy machine name to clipboard
                ColumnDefinition c2t = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.cpyName, GridUnitType.Star) };
                // Machine IP
                ColumnDefinition c3t = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.machIp, GridUnitType.Star) };
                // Wilcom folder
                ColumnDefinition c4t = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.wilcomFolder, GridUnitType.Star) };
                // Wilcom folder open button
                ColumnDefinition colOpenWFldrH = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.openWilcomFldr, GridUnitType.Star) };
                // Wilcom folder copy button
                ColumnDefinition colCpyWFldrH = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.cpyWilcomFolder, GridUnitType.Star) };
                // check active
                ColumnDefinition c6t = new ColumnDefinition
                { Width = new GridLength((int)ColWidth.machAct, GridUnitType.Star) };
                gt.ColumnDefinitions.Add(c1t); //machine name
                gt.ColumnDefinitions.Add(c2t); //cpy machine name
                gt.ColumnDefinitions.Add(c3t); //machine IP
                gt.ColumnDefinitions.Add(c4t); //Wilcom folder
                gt.ColumnDefinitions.Add(colOpenWFldrH); //Wilcom folder open button
                gt.ColumnDefinitions.Add(colCpyWFldrH); //Wilcom folder copy button
                gt.ColumnDefinitions.Add(c6t); //Active

                // 1 - Machine name
                Label nameH = new Label
                {
                    Content = translations.TranslateMsg("Nombre máquina"),
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0, 0, 0, 0),
                    FontWeight = FontWeights.Bold,
                    FontSize = 16,
                };
                nameH.SetValue(Grid.ColumnSpanProperty, 1);
                nameH.SetValue(Grid.ColumnProperty, (int)ColOrder.machName);

                // 2 - cpy machine name (no header)

                // 3 - machine ip
                Label ipH = new Label
                {
                    Content = "IPv4",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0, 0, 0, 0),
                    FontWeight = FontWeights.Bold,
                    FontSize = 16,
                };
                ipH.SetValue(Grid.ColumnSpanProperty, 1);
                ipH.SetValue(Grid.ColumnProperty, (int)ColOrder.machIp);

                // 4 - wilcom folder
                Label wilcomFldr = new Label
                {
                    Content = translations.TranslateMsg("Carpeta wilcom"),
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0, 0, 0, 0),
                    FontWeight = FontWeights.Bold,
                    FontSize = 16,
                };
                wilcomFldr.SetValue(Grid.ColumnSpanProperty, 1);
                wilcomFldr.SetValue(Grid.ColumnProperty, (int)ColOrder.wilcomFolder);

                // 5 - Open wilcom folder(no header)
                // 6 - cpy wilcom folder(no header)

                // 7 - machine active
                Label activeH = new Label
                {
                    Content = "Act",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0, 0, 0, 0),
                    FontWeight = FontWeights.Bold,
                    FontSize = 16,
                };
                activeH.SetValue(Grid.ColumnSpanProperty, 1);
                activeH.SetValue(Grid.ColumnProperty, (int)ColOrder.machAct);

                _ = gt.Children.Add(nameH); // 1 
                _ = gt.Children.Add(ipH); // 3
                _ = gt.Children.Add(wilcomFldr); // 4
                _ = gt.Children.Add(activeH);  //7
                gt.UpdateLayout();
                _ = LBox.Items.Add(gt);
            }

            Grid g = new Grid
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Height = 19,
                Width = gridWidth,
                Margin = new Thickness(0),
                VerticalAlignment = VerticalAlignment.Top,
                ShowGridLines = true,
            };

            // machine name
            ColumnDefinition c1 = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.machName, GridUnitType.Star) };
            // Button to copy machine name to clipboard
            ColumnDefinition c2 = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.cpyName, GridUnitType.Star) };
            // Machine IP
            ColumnDefinition c3 = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.machIp, GridUnitType.Star) };
            // Wilcom folder
            ColumnDefinition c4 = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.wilcomFolder, GridUnitType.Star) };
            // Wilcom folder copy button
            ColumnDefinition copenWFldr = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.openWilcomFldr, GridUnitType.Star) };
            ColumnDefinition c5 = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.cpyWilcomFolder, GridUnitType.Star) };
            ColumnDefinition c6 = new ColumnDefinition
            { Width = new GridLength((int)ColWidth.machAct, GridUnitType.Star) };
            g.ColumnDefinitions.Add(c1); //machine name
            g.ColumnDefinitions.Add(c2); //cpy machine name
            g.ColumnDefinitions.Add(c3); //machine IP
            g.ColumnDefinitions.Add(c4); //Wilcom folder
            g.ColumnDefinitions.Add(copenWFldr); //Wilcom folder open button
            g.ColumnDefinitions.Add(c5); //Wilcom folder copy button
            g.ColumnDefinitions.Add(c6); //check active

            // 1 - Machine name
            TextBox nameTxt = new TextBox
            {
                Text = m.Name,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Margin = new Thickness(10, 0, 0, 0),
                TextWrapping = TextWrapping.Wrap
            };
            nameTxt.SetValue(Grid.ColumnSpanProperty, 1);
            nameTxt.SetValue(Grid.ColumnProperty, (int)ColOrder.machName);

            // 3 - machine ip
            TextBox ipTxt = new TextBox
            {
                Text = m.IP,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Margin = new Thickness(10, 0, 0, 0),
                TextWrapping = TextWrapping.Wrap
            };
            ipTxt.SetValue(Grid.ColumnSpanProperty, 1);
            ipTxt.SetValue(Grid.ColumnProperty, (int)ColOrder.machIp);

            // 4 - wilcom folder
            TextBox wilcomFldrTxt = new TextBox
            {
                Text = m.WFolder, // wilcom folder
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Margin = new Thickness(10, 0, 0, 0),
                TextWrapping = TextWrapping.Wrap
            };
            wilcomFldrTxt.SetValue(Grid.ColumnSpanProperty, 1);
            wilcomFldrTxt.SetValue(Grid.ColumnProperty, (int)ColOrder.wilcomFolder);

            // 6 - machine active
            CheckBox activeChk = new CheckBox
            {
                IsChecked = (m.Active == true ? true : false),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Margin = new Thickness(10, 0, 0, 0),
            };
            activeChk.SetValue(Grid.ColumnSpanProperty, 1);
            activeChk.SetValue(Grid.ColumnProperty, (int)ColOrder.machAct);

            // 2 - cpy machine name 
            Button cpyName = new Button
            {
                Width = 24,
                Height = 24,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri(
                        @"pack://application:,,,/"
                        + Assembly.GetExecutingAssembly().GetName().Name
                        + ";component/"
                        + "/Images/clipboard.png", UriKind.RelativeOrAbsolute)),
                    VerticalAlignment = VerticalAlignment.Center
                },
                ToolTip = translations.TranslateMsg("Copiar al portapales"),
            };
            cpyName.Click += CpyName_Click;
            cpyName.SetValue(Grid.ColumnSpanProperty, 1);
            cpyName.SetValue(Grid.ColumnProperty, (int)ColOrder.cpyName);
            cpyName.Visibility = !Properties.Settings.Default.IsWilcomVersion ? Visibility.Hidden : Visibility.Visible;

            // 5 - open wilcom folder
            Button openWFldr = new Button
            {
                Width = 25,
                Height = 25,
                Padding = new Thickness(0),
                Content = new Image
                {
                    Source = new BitmapImage(new Uri(
                        @"pack://application:,,,/"
                        + Assembly.GetExecutingAssembly().GetName().Name
                        + ";component/"
                        + "/Images/FolderS.png", UriKind.RelativeOrAbsolute)),
                    Height = 15,
                    Width = 15,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(0),
                },
                ToolTip = translations.TranslateMsg("carpeta wilcom"),
            };
            openWFldr.Click += OpenWFldr_Click;
            openWFldr.SetValue(Grid.ColumnSpanProperty, 1);
            openWFldr.SetValue(Grid.ColumnProperty, (int)ColOrder.openWilcomFldr);
            openWFldr.Visibility = !Properties.Settings.Default.IsWilcomVersion ? Visibility.Hidden : Visibility.Visible;
            openWFldr.Visibility = Visibility.Visible; //always visibe 

            // 6 - cpy wilcom folder
            Button cpyWilcomFolder = new Button
            {
                Width = 24,
                Height = 24,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri(
                        @"pack://application:,,,/"
                        + Assembly.GetExecutingAssembly().GetName().Name
                        + ";component/"
                        + "/Images/clipboard.png", UriKind.RelativeOrAbsolute)),
                    VerticalAlignment = VerticalAlignment.Center
                },
                ToolTip = translations.TranslateMsg("Copiar al portapales"),
            };
            cpyWilcomFolder.Click += CpyWilcomFldr_Click;
            cpyWilcomFolder.SetValue(Grid.ColumnSpanProperty, 1);
            cpyWilcomFolder.SetValue(Grid.ColumnProperty, (int)ColOrder.cpyWilcomFolder);
            cpyWilcomFolder.Visibility = !Properties.Settings.Default.IsWilcomVersion ? Visibility.Hidden : Visibility.Visible;

            _ = g.Children.Add(nameTxt); // 1 
            _ = g.Children.Add(cpyName); // 2
            _ = g.Children.Add(ipTxt); // 3
            _ = g.Children.Add(wilcomFldrTxt); // 4
            _ = g.Children.Add(openWFldr); // 5
            _ = g.Children.Add(cpyWilcomFolder); // 6
            _ = g.Children.Add(activeChk); // 7
            g.UpdateLayout();
            _ = LBox.Items.Add(g);

        }

        private void OpenWFldr_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Grid x = button.Parent as Grid;

            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog
            {
                Description = translations.TranslateMsg("Seleccione una carpeta"),
                UseDescriptionForTitle = true // This applies to the Vista style dialog only, not the old dialog.
            };
            if ((bool)dialog.ShowDialog(this))
            {
                TextBox t = (TextBox)x.Children[(int)ColOrder.wilcomFolder];
                t.Text = dialog.SelectedPath;
            }

            Clipboard.SetText(((TextBox)x.Children[(int)ColOrder.wilcomFolder]).Text);

            int duplicado = 0;
            List<string> duplicados = new List<string>();
            foreach (Grid item in LBox.Items)
            {
                if (item.Children.Count >= 6)
                {
                    string WFolder = ((TextBox)item.Children[(int)ColOrder.wilcomFolder]).Text;
                    string WMaquina = ((TextBox)item.Children[(int)ColOrder.machName]).Text;

                    if (WFolder == ((TextBox)x.Children[(int)ColOrder.wilcomFolder]).Text)
                    {
                        duplicado++;
                        duplicados.Add(WMaquina + ":" + WFolder + "\n");
                    }
                }
            }

            // TODO muestra mensaje de folder duplicado pero no toma ninguna accion.
            if (duplicado > 1)
            {
                if (TaskDialog.OSSupportsTaskDialogs)
                {
                    using (TaskDialog d = new TaskDialog())
                    {
                        d.WindowTitle = translations.TranslateMsg("folder duplicado");
                        d.MainInstruction = translations.TranslateMsg("Un folder es utilizado en más de una máquinA");
                        d.Content = String.Join(string.Empty, duplicados);
                        d.ExpandedInformation = translations.TranslateMsg("si no se cambia los archivos se moveran solo a la primera máquina");
                        d.Footer = translations.TranslateMsg("Consulte la ayuda para más información");
                        d.FooterIcon = TaskDialogIcon.Information;
                        d.EnableHyperlinks = true;
                        //TaskDialogButton customButton = new TaskDialogButton("A custom button");
                        TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);
                        //TaskDialogButton cancelButton = new TaskDialogButton(ButtonType.Cancel);
                        //d.Buttons.Add(customButton);
                        d.Buttons.Add(okButton);
                        //d.Buttons.Add(cancelButton);
                        //d.HyperlinkClicked += new EventHandler<HyperlinkClickedEventArgs>(TaskDialog_HyperLinkClicked);
                        TaskDialogButton b = d.ShowDialog(this);
                        //if (b == customButton)
                        //    MessageBox.Show(this, "You clicked the custom button", "Task Dialog Sample");
                        //else if (b == okButton)
                        //    MessageBox.Show(this, "You clicked the OK button.", "Task Dialog Sample");
                    }
                }
                else
                {
                    _ = MessageBox.Show(this, translations.TranslateMsg("Un folder es utilizado en más de una máquina"), translations.TranslateMsg("folder duplicado"));
                }
            }
        }

        private void CpyName_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Grid x = button.Parent as Grid;
            Clipboard.SetText(((TextBox)x.Children[(int)ColOrder.machName]).Text);
        }
        private void CpyWilcomFldr_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Grid x = button.Parent as Grid;
            Clipboard.SetText(((TextBox)x.Children[(int)ColOrder.wilcomFolder]).Text);
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            bool canBeClosed = true; // si no hay ninguna maquina configurada no se deja cerrar
            // solo guarda con CTRL para que el usuario no dañe nada.
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Recorre y valida las maquinas
                List<MachinesMachine> machines = new List<MachinesMachine>();
                foreach (Grid item in LBox.Items)
                {
                    if (item.Children.Count >= 6)
                    {
                        MachinesMachine m = new MachinesMachine
                        {
                            Name = ((TextBox)item.Children[(int)ColOrder.machName]).Text,
                            WFolder = ((TextBox)item.Children[(int)ColOrder.wilcomFolder]).Text,
                            IP = ((TextBox)item.Children[(int)ColOrder.machIp]).Text,
                            Active = ((CheckBox)item.Children[(int)ColOrder.machAct]).IsChecked == true ? true : false
                        };
                        if (!string.IsNullOrEmpty(m.Name) && !string.IsNullOrEmpty(m.IP))
                        {
                            // the ip can be a regular IPv4
                            if (GenericosDG.ValidateIPRegEx(m.IP))
                            {
                                machines.Add(m); //add if ip is valid
                            }
                            else
                            {//or the ip can be a name that responds ping
                                using (Ping p = new Ping())
                                {
                                    PingReply reply = p.Send(m.IP, 500);
                                    if (reply.Status == IPStatus.Success)
                                    {
                                        machines.Add(m); //add if name responds to ping
                                    }
                                }
                            }
                        }
                    }
                }

                // Si hay máquinas válidas las guarda en el archivo de configuración
                if (machines.Count > 0)
                {
                    string machinesCfgFile = SettingsFolder + @"Machines.xml";
                    Machines _m = new Machines
                    {
                        Machine = machines.ToArray()
                    };
                    _ = GenericosDG.ObjToXmlFile(_m, machinesCfgFile);
                    Changes = true;
                }
                else
                {
                    canBeClosed = false;
                }

                // si se cambió o configuró folder para integracion EMB
                if (EMBIntegration != EMBIntegrationFldr.Text)
                {
                    EMBIntegration = EMBIntegrationFldr.Text;
                    Changes = true;
                }
                // cambio en el modo operador
                if (isOperatorFlg != (bool)chkOperator.IsChecked)
                {
                    isOperatorFlg = (bool)chkOperator.IsChecked;
                    Changes = true;
                }
                // cambio en el uso de TSPro
                if (useTSProFlag != (bool)chkTruesizerpro.IsChecked)
                {
                    useTSProFlag = (bool)chkTruesizerpro.IsChecked;
                    Changes = true;
                }
            }
            if (canBeClosed)
            {
                Close();
            }
        }

        private void AddMachine_Click(object sender, RoutedEventArgs e)
        {
            MachinesMachine m = new MachinesMachine
            {
                Name = "",
                IP = "",
                Active = true
            };
            AddMachine(m);
        }

        private void StartIP_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateIPRange();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateIPRange();
        }

        private void ValidateIPRange()
        {// Verificar que las dos ip son validas y son secuencia.
            if (GenericosDG.ValidateIPRegEx(startIP.Text) && GenericosDG.ValidateIPRegEx(endIP.Text))
            {
                string[] segmentsIni = startIP.Text.Split('.');
                string[] segmentsEnd = endIP.Text.Split('.');
                if (segmentsIni[0] == segmentsEnd[0] &&
                    segmentsIni[1] == segmentsEnd[1] &&
                    segmentsIni[2] == segmentsEnd[2] &&
                  int.Parse(segmentsIni[3]) < int.Parse(segmentsEnd[3]))
                {
                    btnScan.IsEnabled = true;
                }
                else
                {
                    btnScan.IsEnabled = false;
                }
            }
        }

        private void StartIP_LostFocus(object sender, RoutedEventArgs e)
        {
            if (GenericosDG.ValidateIPRegEx(startIP.Text) && string.IsNullOrEmpty(endIP.Text))
            {
                string[] segments = startIP.Text.Split('.');
                endIP.Text = segments[0] + "." + segments[1] + "." + segments[2] + ".255";
            }
        }

        private void BtnScan_Click(object sender, RoutedEventArgs e)
        {
            //Do range sacan
            btnScan.Content = translations.TranslateMsg("ESPERE");
            btnScan.IsEnabled = false;

            try
            {
                List<string> ips = RunNetScan(startIP.Text, endIP.Text);

                foreach (var item in ips)
                {
                    if (!AlreadyAdded(item)) //check for duplicates
                    {
                        MachinesMachine m = new MachinesMachine
                        {
                            Active = true,
                            IP = item,
                            Name = "M" + item.Split('.')[3] //el nombre de la maquina no se puede opbtener. Toca poner algo y que el usuario lo cambie
                        };
                        AddMachine(m);
                    }
                }
                btnScan.Content = translations.TranslateMsg("Buscar máquinas");
                btnScan.IsEnabled = true;
            }
            catch (Exception ex)
            {
                _ = GenericosDG.Add2DebugLog($"  error : {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
            }
        }

        private bool AlreadyAdded(string item)
        {
            foreach (Grid _item in LBox.Items)
            {
                if (_item.Children.Count == 7) // this identifies the row that is not the title
                {
                    TextBox x = _item.Children[(int)ColOrder.machIp] as TextBox;
                    if (x.Text == item)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //https://gist.github.com/jonlabelle/b1c007ee387b1bee1b8677224c57bc6c
        public List<string> RunNetScan(string stIP, string endIP)
        {
            List<string> DetectedCxD;

            // do a specific range
            const int SEG_MIN_VALUE = 1;
            const int SEG_MAX_VALUE = 255;

            stIP = !string.IsNullOrEmpty(stIP) ? stIP : "\\";

            int minSeg = !string.IsNullOrEmpty(stIP) ? int.Parse(stIP.Split('.')[3]) : 1;
            int maxSeg = !string.IsNullOrEmpty(endIP) ? int.Parse(endIP.Split('.')[3]) : 1;


            //Handle bad calls
            {
                if (minSeg > maxSeg)
                {
                    throw new ArgumentException("Min segment cannot be greater than max segment");
                }

                if (minSeg < SEG_MIN_VALUE || minSeg > SEG_MAX_VALUE)
                {
                    throw new ArgumentOutOfRangeException(
                        $"Min segment cannot be less than {SEG_MIN_VALUE} " +
                        $"or greater than {SEG_MAX_VALUE}");
                }

                if (maxSeg < SEG_MIN_VALUE || maxSeg > SEG_MAX_VALUE)
                {
                    throw new ArgumentOutOfRangeException(
                        $"Max segment cannot be less than {SEG_MIN_VALUE} " +
                        $"or greater than {SEG_MAX_VALUE}");
                }
            }

            //will return this list of detected cxd
            DetectedCxD = new List<string>();

            txtScanResult.Inlines.Add(new Run("Scanning... wait " + "\n") { Foreground = Brushes.Red });

            //txtScanResult.Background = Brushes.AntiqueWhite;
            //txtScanResult.Foreground = Brushes.Navy;
            //txtScanResult.FontFamily = new FontFamily("Century Gothic");
            //txtScanResult.FontSize = 12;
            //txtScanResult.FontStretch = FontStretches.UltraExpanded;
            //txtScanResult.FontStyle = FontStyles.Italic;
            //txtScanResult.FontWeight = FontWeights.UltraBold;
            //txtScanResult.LineHeight = Double.NaN;
            //txtScanResult.Padding = new Thickness(5, 10, 5, 10);
            //txtScanResult.TextAlignment = TextAlignment.Center;
            //txtScanResult.TextWrapping = TextWrapping.Wrap;
            //txtScanResult.Typography.NumeralStyle = FontNumeralStyle.OldStyle;
            //txtScanResult.Typography.SlashedZero = true;foreach (var itm5 in "! Hello World !; %Hello World%".Split(';'))

            // remove event textChanged
            startIP.TextChanged -= StartIP_TextChanged;

            //ip prefix
            string _ip = stIP.Split('.')[0] + "." + stIP.Split('.')[1] + "." + stIP.Split('.')[2] + ".";

            //loop throug all ips on given range
            for (int segment = minSeg; segment <= maxSeg; segment++)
            {
                startIP.Text = _ip + segment.ToString().Trim();
                txtScanResult.Inlines.Add(new Run("Scanning: " + startIP.Text + "\n") { Foreground = Brushes.Blue });
                Do_Events();
                if (IsIPCxD(_ip + segment.ToString().Trim()))
                {
                    // if we got here it is a CxD
                    DetectedCxD.Add(_ip + segment.ToString().Trim());
                    txtScanResult.Inlines.Add(new Run("Found: " + _ip + segment.ToString().Trim() + "\n\r") { Foreground = Brushes.Green });
                }
                ScanScrollViewer.ScrollToEnd();
            }
            // re-add event textChanged
            startIP.TextChanged += StartIP_TextChanged;
            return DetectedCxD;
        }

        /// <summary>
        /// WPF no tiene doEvents y toca hacer esto.
        /// Sin doEvents no se ven los cambios en la ventana mientras se escanea
        /// el rango de IPs
        /// </summary>
        private static void Do_Events()
        {
            _ = Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));
        }

        private bool IsIPCxD(string ip)
        {
            try
            {
                using (Ping p = new Ping())
                {
                    PingReply reply = p.Send(ip, 500);

                    if (reply.Status == IPStatus.Success)
                    {
                        txtScanResult.Inlines.Add(new Run(" - Ping OK: " + reply.RoundtripTime.ToString() + " ms\n") { Foreground = Brushes.Green });

                        string mac = GenericosDG.GetMacAddress(ip);
                        txtScanResult.Inlines.Add(new Run(" - id: " + mac + "\n") { Foreground = Brushes.Blue });

                        if (Properties.Settings.Default.Mac.Contains(mac.Substring(0, 8)))
                        {
                            txtScanResult.Inlines.Add(new Run(" - is CxD: OK" + "\n") { Foreground = Brushes.Blue });
                            return true;
                        }
                        txtScanResult.Inlines.Add(new Run(" - is CxD: Fail" + "\n") { Foreground = Brushes.Red });
                        return false;
                    }
                    txtScanResult.Inlines.Add(new Run(" - Ping Fail: " + reply.Status.ToString() + " \n") { Foreground = Brushes.Red });
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        private void OpenEMBFolder_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog
            {
                Description = translations.TranslateMsg("Seleccione una carpeta"),
                UseDescriptionForTitle = true // This applies to the Vista style dialog only, not the old dialog.
            };
            if ((bool)dialog.ShowDialog(this))
            {
                EMBIntegrationFldr.Text = dialog.SelectedPath;
            }
        }
    }
}



