﻿#pragma warning disable CA1051 // Do not declare visible instance fields
#pragma warning disable CA1401 // P/Invokes should not be visible
#pragma warning disable CA1815 // Override equals and operator equals on value types
#pragma warning disable CA1060 // Move pinvokes to native methods class
using System;
using System.Runtime.InteropServices;

namespace PreviewHost.Interop
{
    [StructLayout(LayoutKind.Sequential)]
    public struct AcceleratorEntry
    {
        public byte IsVirtual;
        public ushort Key;
        public ushort Command;

        [DllImport("user32.dll", CharSet = CharSet.Unicode, PreserveSig = true, SetLastError = true)]
        public static extern IntPtr CreateAcceleratorTable([In, MarshalAs(UnmanagedType.LPArray)] AcceleratorEntry[] paccel, int cAccel);
    }
}
