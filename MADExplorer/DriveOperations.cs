﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MADExplorer
{
    class DriveOperations
    {
    //    private string GetDriveNetworkLabel(DriveInfo drive)
    //    {

    //        string TempNetwork;
    //        string SecondTempNetwork;
    //        RegistryKey rkTest = Registry.CurrentUser.OpenSubKey("RegistryOpenSubKeyExample");
    //        TempNetwork = strings.Replace(Computer.Registry.GetValue(@"HKEY_CURRENT_USER\Network\" + drive.Name.Substring(0,1) , "RemotePath", ""), "\", "#");
    //    SecondTempNetwork = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\" & TempNetwork, "_LabelFromReg", "");

    //    return SecondTempNetwork

    //}

        //*************************************************
        public const string SHELL = "shell32.dll";
        [DllImport(SHELL, CharSet = CharSet.Unicode)]
        public static extern uint SHParseDisplayName(string pszName, IntPtr zero, [Out] out IntPtr ppidl, uint sfgaoIn, [Out] out uint psfgaoOut);

        [DllImport(SHELL, CharSet = CharSet.Unicode)]
        public static extern uint SHGetNameFromIDList(IntPtr pidl, SIGDN sigdnName, [Out] out String ppszName);
        public enum SIGDN : uint
        {
            NORMALDISPLAY = 0x00000000,
            PARENTRELATIVEPARSING = 0x80018001,
            DESKTOPABSOLUTEPARSING = 0x80028000,
            PARENTRELATIVEEDITING = 0x80031001,
            DESKTOPABSOLUTEEDITING = 0x8004c000,
            FILESYSPATH = 0x80058000,
            URL = 0x80068000,
            PARENTRELATIVEFORADDRESSBAR = 0x8007c001,
            PARENTRELATIVE = 0x80080001
        }

        //var x = GetDriveLabel(@"C:\")
        public static string GetDriveLabel(string driveNameAsLetterColonBackslash)
        {
            IntPtr pidl;
            uint dummy;
            string name;
            if (SHParseDisplayName(driveNameAsLetterColonBackslash, IntPtr.Zero, out pidl, 0, out dummy) == 0
                && SHGetNameFromIDList(pidl, SIGDN.PARENTRELATIVEEDITING, out name) == 0
                && name != null)
            {
                return name;
            }
            return null;
        }

        //***********************************************


        public static bool CheckDriveIsCXD(DriveInfo di, out string machinName)
        {
            string drive = di.Name;
            var sb = new StringBuilder(512);
            var size = sb.Capacity;
            var error = NativeMethods.WNetGetConnection(drive.Substring(0, 2), sb, ref size);
            if (error != 0) //error es diferente de cero si no es una unidad de red.
            {
                machinName = String.Empty;
                return false; //throw new Win32Exception(error, "WNetGetConnection failed");
            }
            var networkpath = sb.ToString();

            CultureInfo cul = CultureInfo.InstalledUICulture;

            //if (networkpath.Contains("DavWWWRoot"))
            if (cul.CompareInfo.IndexOf(networkpath, "DavWWWRoot", CompareOptions.IgnoreCase) >= 0)
            {
                // machinName = networkpath.Split('\\')[2];
                machinName = GetDriveLabel(drive);
                machinName = machinName.ToUpper().Replace("\\", "").Replace("DAVWWWROOT", "").Replace("(","").Replace(")","");
                return true;
            }
            machinName = String.Empty;
            return false;
        }



    }
}
