﻿namespace MADCXDInit
{
    partial class MADCXDInit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MADCXDInit));
            this.lblWelcome = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.errorProviderDriveSel = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.espanolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLang = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLanguage = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtMachineName = new System.Windows.Forms.TextBox();
            this.errorProviderMachineName = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnStepMacineName = new System.Windows.Forms.Panel();
            this.txtIP = new IPAddressControlLib.IPAddressControl();
            this.lblWIfipass = new System.Windows.Forms.Label();
            this.lblSSID = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtSSID = new System.Windows.Forms.TextBox();
            this.lblIP = new System.Windows.Forms.Label();
            this.lblMachineName = new System.Windows.Forms.Label();
            this.errorProviderMachName = new System.Windows.Forms.ErrorProvider(this.components);
            this.appData = new MADCXDInitLNG.AppData();
            this.BtnWriteCfg = new System.Windows.Forms.Button();
            this.lblTarget = new System.Windows.Forms.Label();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.lblSuccess = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDriveSel)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderMachineName)).BeginInit();
            this.pnStepMacineName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderMachName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appData)).BeginInit();
            this.SuspendLayout();
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(22, 25);
            this.lblWelcome.MinimumSize = new System.Drawing.Size(760, 0);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(760, 17);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Bienvenido a la utilidad de inicialización de su nuevo didspoitivo de Conxión Dir" +
    "ecta de MAD Ingenieros.";
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "CD.ico");
            this.imageList1.Images.SetKeyName(1, "USB2.png");
            this.imageList1.Images.SetKeyName(2, "DVD.ico");
            this.imageList1.Images.SetKeyName(3, "Floppy35.ico");
            this.imageList1.Images.SetKeyName(4, "Floppy50.ico");
            this.imageList1.Images.SetKeyName(5, "HD.ico");
            this.imageList1.Images.SetKeyName(6, "MAD.ico");
            this.imageList1.Images.SetKeyName(7, "MAD.png");
            this.imageList1.Images.SetKeyName(8, "NetDisconnected.ico");
            this.imageList1.Images.SetKeyName(9, "Network.ico");
            this.imageList1.Images.SetKeyName(10, "SD.ico");
            this.imageList1.Images.SetKeyName(11, "SSD.ico");
            this.imageList1.Images.SetKeyName(12, "Tape.ico");
            this.imageList1.Images.SetKeyName(13, "Win.ico");
            this.imageList1.Images.SetKeyName(14, "ZIP.ico");
            this.imageList1.Images.SetKeyName(15, "CXD.png");
            this.imageList1.Images.SetKeyName(16, "USB.png");
            // 
            // errorProviderDriveSel
            // 
            this.errorProviderDriveSel.ContainerControl = this;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(885, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageToolStripMenuItem,
            this.errorLogToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 25);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishToolStripMenuItem,
            this.espanolToolStripMenuItem});
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            this.languageToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.languageToolStripMenuItem.Text = "Language";
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            this.englishToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.englishToolStripMenuItem.Text = "English";
            this.englishToolStripMenuItem.Click += new System.EventHandler(this.EnglishToolStripMenuItem_Click_1);
            // 
            // espanolToolStripMenuItem
            // 
            this.espanolToolStripMenuItem.CheckOnClick = true;
            this.espanolToolStripMenuItem.Name = "espanolToolStripMenuItem";
            this.espanolToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.espanolToolStripMenuItem.Text = "Español";
            this.espanolToolStripMenuItem.Click += new System.EventHandler(this.EspanolToolStripMenuItem_Click_1);
            // 
            // errorLogToolStripMenuItem
            // 
            this.errorLogToolStripMenuItem.Name = "errorLogToolStripMenuItem";
            this.errorLogToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.errorLogToolStripMenuItem.Text = "ErrorLog";
            this.errorLogToolStripMenuItem.Click += new System.EventHandler(this.ErrorLogToolStripMenuItem_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.AllowItemReorder = true;
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLang,
            this.toolStripStatusLanguage});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 329);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(885, 22);
            this.StatusStrip1.TabIndex = 6;
            this.StatusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLang
            // 
            this.toolStripStatusLang.Name = "toolStripStatusLang";
            this.toolStripStatusLang.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLang.Text = "Language";
            // 
            // toolStripStatusLanguage
            // 
            this.toolStripStatusLanguage.Name = "toolStripStatusLanguage";
            this.toolStripStatusLanguage.Size = new System.Drawing.Size(57, 17);
            this.toolStripStatusLanguage.Text = "----------";
            this.toolStripStatusLanguage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ToolStripStatusLanguage_MouseDown);
            // 
            // txtMachineName
            // 
            this.txtMachineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMachineName.Location = new System.Drawing.Point(172, 11);
            this.txtMachineName.Name = "txtMachineName";
            this.txtMachineName.Size = new System.Drawing.Size(194, 23);
            this.txtMachineName.TabIndex = 1;
            this.txtMachineName.TextChanged += new System.EventHandler(this.TxtMachineName_TextChanged);
            this.txtMachineName.Validating += new System.ComponentModel.CancelEventHandler(this.TxtMachineName_Validating);
            // 
            // errorProviderMachineName
            // 
            this.errorProviderMachineName.ContainerControl = this;
            // 
            // pnStepMacineName
            // 
            this.pnStepMacineName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnStepMacineName.Controls.Add(this.txtIP);
            this.pnStepMacineName.Controls.Add(this.lblWIfipass);
            this.pnStepMacineName.Controls.Add(this.lblSSID);
            this.pnStepMacineName.Controls.Add(this.txtPassword);
            this.pnStepMacineName.Controls.Add(this.txtSSID);
            this.pnStepMacineName.Controls.Add(this.lblIP);
            this.pnStepMacineName.Controls.Add(this.lblMachineName);
            this.pnStepMacineName.Controls.Add(this.txtMachineName);
            this.pnStepMacineName.Location = new System.Drawing.Point(25, 68);
            this.pnStepMacineName.Name = "pnStepMacineName";
            this.pnStepMacineName.Size = new System.Drawing.Size(760, 138);
            this.pnStepMacineName.TabIndex = 12;
            // 
            // txtIP
            // 
            this.txtIP.AllowInternalTab = false;
            this.txtIP.AutoHeight = true;
            this.txtIP.BackColor = System.Drawing.SystemColors.Window;
            this.txtIP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtIP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIP.Location = new System.Drawing.Point(172, 38);
            this.txtIP.MinimumSize = new System.Drawing.Size(114, 23);
            this.txtIP.Name = "txtIP";
            this.txtIP.ReadOnly = false;
            this.txtIP.Size = new System.Drawing.Size(114, 23);
            this.txtIP.TabIndex = 2;
            this.txtIP.Text = "...";
            // 
            // lblWIfipass
            // 
            this.lblWIfipass.AutoSize = true;
            this.lblWIfipass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWIfipass.Location = new System.Drawing.Point(15, 95);
            this.lblWIfipass.Name = "lblWIfipass";
            this.lblWIfipass.Size = new System.Drawing.Size(109, 17);
            this.lblWIfipass.TabIndex = 1;
            this.lblWIfipass.Text = "Clave de la WiFi";
            // 
            // lblSSID
            // 
            this.lblSSID.AutoSize = true;
            this.lblSSID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSSID.Location = new System.Drawing.Point(15, 68);
            this.lblSSID.Name = "lblSSID";
            this.lblSSID.Size = new System.Drawing.Size(124, 17);
            this.lblSSID.TabIndex = 1;
            this.lblSSID.Text = "Nombre de la WiFi";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(172, 92);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(194, 23);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.TextChanged += new System.EventHandler(this.TxtPassword_TextChanged);
            // 
            // txtSSID
            // 
            this.txtSSID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSSID.Location = new System.Drawing.Point(172, 65);
            this.txtSSID.Name = "txtSSID";
            this.txtSSID.Size = new System.Drawing.Size(194, 23);
            this.txtSSID.TabIndex = 4;
            this.txtSSID.TextChanged += new System.EventHandler(this.TxtSSID_TextChanged);
            // 
            // lblIP
            // 
            this.lblIP.AutoSize = true;
            this.lblIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIP.Location = new System.Drawing.Point(15, 41);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(41, 17);
            this.lblIP.TabIndex = 1;
            this.lblIP.Text = "Fix IP";
            // 
            // lblMachineName
            // 
            this.lblMachineName.AutoSize = true;
            this.lblMachineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMachineName.Location = new System.Drawing.Point(15, 14);
            this.lblMachineName.Name = "lblMachineName";
            this.lblMachineName.Size = new System.Drawing.Size(151, 17);
            this.lblMachineName.TabIndex = 1;
            this.lblMachineName.Text = "Nombre de la máquina";
            // 
            // errorProviderMachName
            // 
            this.errorProviderMachName.ContainerControl = this;
            // 
            // appData
            // 
            this.appData.DataSetName = "AppData";
            this.appData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // BtnWriteCfg
            // 
            this.BtnWriteCfg.Enabled = false;
            this.BtnWriteCfg.Location = new System.Drawing.Point(535, 265);
            this.BtnWriteCfg.Name = "BtnWriteCfg";
            this.BtnWriteCfg.Size = new System.Drawing.Size(75, 23);
            this.BtnWriteCfg.TabIndex = 13;
            this.BtnWriteCfg.Text = "Write Config";
            this.BtnWriteCfg.UseVisualStyleBackColor = true;
            this.BtnWriteCfg.Click += new System.EventHandler(this.BtnWriteCfg_Click);
            // 
            // lblTarget
            // 
            this.lblTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTarget.Location = new System.Drawing.Point(23, 268);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(201, 20);
            this.lblTarget.TabIndex = 1;
            this.lblTarget.Text = "Write to:";
            // 
            // txtTarget
            // 
            this.txtTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTarget.Location = new System.Drawing.Point(230, 265);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(289, 23);
            this.txtTarget.TabIndex = 5;
            this.txtTarget.TextChanged += new System.EventHandler(this.TxtPassword_TextChanged);
            // 
            // lblSuccess
            // 
            this.lblSuccess.AutoSize = true;
            this.lblSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuccess.Location = new System.Drawing.Point(164, 302);
            this.lblSuccess.Name = "lblSuccess";
            this.lblSuccess.Size = new System.Drawing.Size(113, 17);
            this.lblSuccess.TabIndex = 1;
            this.lblSuccess.Text = "Guardar exitoso.";
            this.lblSuccess.Visible = false;
            // 
            // MADCXDInit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 351);
            this.Controls.Add(this.BtnWriteCfg);
            this.Controls.Add(this.lblSuccess);
            this.Controls.Add(this.lblTarget);
            this.Controls.Add(this.pnStepMacineName);
            this.Controls.Add(this.txtTarget);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lblWelcome);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MADCXDInit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MAD CXD Init";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MADCXDInit_FormClosing);
            this.Load += new System.EventHandler(this.MADCXDInit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDriveSel)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderMachineName)).EndInit();
            this.pnStepMacineName.ResumeLayout(false);
            this.pnStepMacineName.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderMachName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ErrorProvider errorProviderDriveSel;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem espanolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorLogToolStripMenuItem;
        private System.Windows.Forms.StatusStrip StatusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLang;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLanguage;
        private MADCXDInitLNG.AppData appData;
        private System.Windows.Forms.TextBox txtMachineName;
        private System.Windows.Forms.ErrorProvider errorProviderMachineName;
        private System.Windows.Forms.Panel pnStepMacineName;
        private System.Windows.Forms.Label lblWIfipass;
        private System.Windows.Forms.Label lblSSID;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtSSID;
        private System.Windows.Forms.Label lblMachineName;
        private System.Windows.Forms.ErrorProvider errorProviderMachName;
        private System.Windows.Forms.Label lblIP;
        private IPAddressControlLib.IPAddressControl txtIP;
        private System.Windows.Forms.Button BtnWriteCfg;
        private System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.TextBox txtTarget;
        private System.Windows.Forms.Label lblSuccess;
    }
}

