﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using GenericosDG;
using MADCXDInitLNG;

namespace MADCXDInit
{
    public partial class Thanks : Form
    {
        // declare Resource manager to access to specific cultureinfo
        CultureInfo cul;            // declare culture info

        public Thanks()
        {
            InitializeComponent();
        }

        private void Thanks_Load(object sender, EventArgs e)
        {
            SwitchLanguage();
            Application.DoEvents();
            Thread.Sleep(3000);
            timer1.Start();
        }

        private void SwitchLanguage()
        {
            Add2SessionLog("SwitchLanguage ");

            //what is on the settings
            string currentCulture = Properties.Settings.Default.Lang;

            // if it is a first time run
            if (currentCulture == string.Empty)
            {
                currentCulture = CultureInfo.CurrentCulture.ToString().Substring(0, 2);
                Add2SessionLog("First Run, culture " + currentCulture.ToString());
            }

            if (currentCulture == "en")         //in english
            {
            }
            else if (currentCulture == "es")
            {
            }
            else //defaults to english
            {
                currentCulture = "en";
            }
            cul = new CultureInfo(currentCulture);
            Add2SessionLog("Language set to " + cul.ToString());

            SetLanguage();
        }
        private void SetLanguage()
        {
            LoadTranslations();

            Thread.CurrentThread.CurrentCulture = cul;
            Thread.CurrentThread.CurrentUICulture = cul;

            try
            {
                string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
                ResourceManager rm = ResourceManager.CreateFileBasedResourceManager("resource", fld, null);

                this.Text = rm.GetString("ProgramName");

                label1.Text = rm.GetString("Thanks1");
                label2.Text = rm.GetString("Thanks2");
                label3.Text = rm.GetString("Thanks3");
                label4.Text = rm.GetString("Thanks4");
                
                rm.ReleaseAllResources();

            }
            catch (Exception)
            {
                Add2ErrorLog("Failed to load lang resources, SetLanguage");
            }
        }

        private string TranslateMsg(string key)
        {
            LoadTranslations();

            string result;

            try
            {
                Thread.CurrentThread.CurrentCulture = cul;
                Thread.CurrentThread.CurrentUICulture = cul;
                string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
                ResourceManager rm = ResourceManager.CreateFileBasedResourceManager("resource", fld, null);

                result = rm.GetString(key);
                rm.ReleaseAllResources();
            }
            catch (Exception)
            {

                result = "String not found " + key;
            }

            return result;
        }

        /// <summary>
        /// Carga las traducciones desde el archivo xml
        /// </summary>
        private void LoadTranslations()
        {
            if (appData.Tables[0].Rows.Count != 0)
            {
                return;
            }

            try
            {
                appData.ReadXml(string.Format("{0}/data.xml", Application.StartupPath));
            }
            catch (Exception)
            {
                Add2ErrorLog("Cant read data.xml" + Application.StartupPath);
            }

            string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
            try
            {
                GenericosDG.GenericosDG.CreateDirectory(fld, true);
            }
            catch (Exception)
            {
                Add2ErrorLog("Can't create folder for resurces : " + fld);
            }

            try
            {
                ResourceWriter ren = new ResourceWriter(fld + "resource.en.resources");
                ResourceWriter res = new ResourceWriter(fld + "resource.es.resources");

                foreach (AppData.LanguagesRow row in appData.Languages.Rows)
                {
                    ren.AddResource(row.ID, row.English);
                    res.AddResource(row.ID, row.Espanol);
                }
                ren.Generate(); ren.Close();
                res.Generate(); res.Close();
            }
            catch (Exception)
            {
                Add2ErrorLog("Can't write resources at : " + fld);
            }
        }
        #region LogHandling
        private void Add2SessionLog(string v)
        {
            while (!GenericosDG.GenericosDG.Add2SessionLog(v)) ;
        }
        private void Add2ErrorLog(string v)
        {
            while (!GenericosDG.GenericosDG.Add2DebugLog(v)) ;
        }
        #endregion

        private void Timer1_Tick(object sender, EventArgs e)
        {
            this.Opacity -= .01;
            Application.DoEvents();
            if (this.Opacity<=0)
            {
                this.Close();
            }
        }
    }
}
