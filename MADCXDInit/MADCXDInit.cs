﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Speech.Synthesis;
using System.Globalization;
using System.Threading;
using System.Resources;
using MADCXDInitLNG;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MADCXDInit
{
    public partial class MADCXDInit : Form
    {
        // declare Resource manager to access to specific cultureinfo
        CultureInfo cul;            // declare culture info

        public MADCXDInit()
        {
            InitializeComponent();
            txtTarget.Text = @"\\192.168.0.200\CONFIG_NEW";
        }

        private void MADCXDInit_Load(object sender, EventArgs e)
        {
            SwitchLanguage();
        }


        #region lenguage

        private void SwitchLanguage()
        {
            Add2SessionLog("SwitchLanguage ");

            //what is on the settings
            string currentCulture = Properties.Settings.Default.Lang;

            // if it is a first time run
            if (currentCulture == string.Empty)
            {
                currentCulture = CultureInfo.CurrentCulture.ToString().Substring(0, 2);
                Add2SessionLog("First Run, culture " + currentCulture.ToString());
            }

            if (currentCulture == "en")         //in english
            {
                englishToolStripMenuItem.Checked = true;
                toolStripStatusLanguage.Text = "English";
            }
            else if (currentCulture == "es")
            {
                espanolToolStripMenuItem.Checked = true;   // español
                toolStripStatusLanguage.Text = "Español";
            }
            else //defaults to english
            {
                englishToolStripMenuItem.Checked = true;
                toolStripStatusLanguage.Text = "English";
                currentCulture = "en";
            }
            cul = new CultureInfo(currentCulture);
            Add2SessionLog("Language set to " + cul.ToString());

            SetLanguage();
        }
        private void SetLanguage()
        {
            LoadTranslations();

            Thread.CurrentThread.CurrentCulture = cul;
            Thread.CurrentThread.CurrentUICulture = cul;

            try
            {
                string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
                ResourceManager rm = ResourceManager.CreateFileBasedResourceManager("resource", fld, null);

                this.Text = rm.GetString("ProgramName");

                lblWelcome.Text = rm.GetString("lblWelcome");
                lblMachineName.Text = rm.GetString("lblMachineName");
                lblIP.Text = rm.GetString("lblIP");
                lblSSID.Text = rm.GetString("lblSSID");
                lblWIfipass.Text = rm.GetString("lblWIfipass");

                toolStripStatusLang.Text = rm.GetString("Language");

                languageToolStripMenuItem.Text = rm.GetString("Language");
                settingsToolStripMenuItem.Text = rm.GetString("Settings");

                BtnWriteCfg.Text = rm.GetString("WriteCfg");
                lblTarget.Text = rm.GetString("WriteTo");

                rm.ReleaseAllResources();

            }
            catch (Exception)
            {
                Add2ErrorLog("Failed to load lang resources, SetLanguage");
            }
        }

        private string TranslateMsg(string key)
        {
            LoadTranslations();

            string result;

            try
            {
                Thread.CurrentThread.CurrentCulture = cul;
                Thread.CurrentThread.CurrentUICulture = cul;
                string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
                ResourceManager rm = ResourceManager.CreateFileBasedResourceManager("resource", fld, null);

                result = rm.GetString(key);
                rm.ReleaseAllResources();
            }
            catch (Exception)
            {

                result = "String not found " + key;
            }

            return result;
        }

        /// <summary>
        /// Carga las traducciones desde el archivo xml
        /// </summary>
        private void LoadTranslations()
        {
            if (appData.Tables[0].Rows.Count != 0)
            {
                return;
            }

            try
            {
                appData.ReadXml(string.Format("{0}/data.xml", Application.StartupPath));
            }
            catch (Exception)
            {
                Add2ErrorLog("Cant read data.xml" + Application.StartupPath);
            }

            string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
            try
            {
                GenericosDG.GenericosDG.CreateDirectory(fld, true);
            }
            catch (Exception)
            {
                Add2ErrorLog("Can't create folder for resurces : " + fld);
            }

            try
            {
                ResourceWriter ren = new ResourceWriter(fld + "resource.en.resources");
                ResourceWriter res = new ResourceWriter(fld + "resource.es.resources");

                foreach (AppData.LanguagesRow row in appData.Languages.Rows)
                {
                    ren.AddResource(row.ID, row.English);
                    res.AddResource(row.ID, row.Espanol);
                }
                ren.Generate(); ren.Close();
                res.Generate(); res.Close();
            }
            catch (Exception)
            {
                Add2ErrorLog("Can't write resources at : " + fld);
            }
        }

        private void EnglishToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            englishToolStripMenuItem.Checked = true;
            espanolToolStripMenuItem.Checked = false;
            Properties.Settings.Default.Lang = "en";
            SwitchLanguage();
        }

        private void EspanolToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            englishToolStripMenuItem.Checked = false;
            espanolToolStripMenuItem.Checked = true;
            Properties.Settings.Default.Lang = "es";
            SwitchLanguage();
        }

        private void ToolStripStatusLanguage_MouseDown(object sender, MouseEventArgs e)
        {
            if (ModifierKeys == Keys.Control)
            {
                using (ConfigLanguage cfgLng = new ConfigLanguage())
                {
                    cfgLng.ShowDialog();
                }
                SwitchLanguage();
            }
        }

        #endregion

        #region LogHandling
        private void Add2SessionLog(string v)
        {
            while (!GenericosDG.GenericosDG.Add2SessionLog(v)) ;
        }
        private void Add2ErrorLog(string v)
        {
            while (!GenericosDG.GenericosDG.Add2DebugLog(v)) ;
        }
        #endregion



        private bool WriteConfig()
        {
            lblSuccess.Text = TranslateMsg("wait");
            lblSuccess.Visible = true;

            Application.DoEvents();
            try
            {
                string cfgtxt = File.ReadAllText(string.Format("{0}/ConfigCXD.txt", Application.StartupPath));

                cfgtxt = cfgtxt.Replace("%machine%", txtMachineName.Text);
                cfgtxt = cfgtxt.Replace("%pass%", txtPassword.Text);
                cfgtxt = cfgtxt.Replace("%SSID%", txtSSID.Text);
                cfgtxt = cfgtxt.Replace("%FixIP%", txtIP.Text);

                string path = txtTarget.Text;

                // Create a file to write to.
                var task = new Task<bool>(() => { var fi = new FileInfo(path); return fi.Exists; });
                task.Start();
                if (task.Wait(100) && task.Result)
                {
                    File.Delete(path);
                }
                File.WriteAllText(path, cfgtxt);

                lblSuccess.Text = TranslateMsg("success");

                return true;
            }
            catch (Exception ex)
            {
                lblSuccess.Text = TranslateMsg("failure") + " " + ex.Message;

                return false;
            }
        }

        private void MADCXDInit_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Visible = false;
            using (Thanks tks = new Thanks())
            {
                tks.ShowDialog();
            }
            //AutoClosingMessageBox.Show(TranslateMsg("Thanks1"), "MAD INGENIEROS", 4000);
        }
        private void ErrorLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenericosDG.GenericosDG.ViewTodaysErrorLog();
        }

        private void TxtMachineName_TextChanged(object sender, EventArgs e)
        {
            if (txtMachineName.Text == String.Empty ||
               txtMachineName.Text.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) != -1)
            {
                errorProviderMachName.SetError(txtMachineName, TranslateMsg("ErrorNombreMaquina"));
            }
            else
            {
                errorProviderMachName.SetError(txtMachineName, String.Empty);
            }
            BtnWriteCfg.Enabled = CheckMachineParameters();
        }

        private bool CheckMachineParameters()
        {
            if (errorProviderMachName.GetError(txtMachineName) == String.Empty &&
                errorProviderMachName.GetError(txtSSID) == String.Empty &&
                errorProviderMachName.GetError(txtPassword) == String.Empty
                && txtMachineName.Text != String.Empty
                && txtSSID.Text != String.Empty
                && txtPassword.Text != String.Empty
                )
            {
                Properties.Settings.Default.SIID = txtSSID.Text;
                Properties.Settings.Default.NetPassword = txtPassword.Text;
                return true;
            }
            return false;
        }

        private void TxtSSID_TextChanged(object sender, EventArgs e)
        {
            if (txtSSID.Text == String.Empty)
            {
                errorProviderMachName.SetError(txtSSID, TranslateMsg("ErrorNombreSSID"));
            }
            else
            {
                errorProviderMachName.SetError(txtSSID, String.Empty);
            }

            BtnWriteCfg.Enabled = CheckMachineParameters();
        }

        private void TxtPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtPassword.Text == String.Empty || txtPassword.Text.Length < 8)
            {
                errorProviderMachName.SetError(txtPassword, TranslateMsg("ErrorPassword"));
            }
            else
            {
                errorProviderMachName.SetError(txtPassword, String.Empty);
            }

            BtnWriteCfg.Enabled = CheckMachineParameters();
        }

        private void TxtMachineName_Validating(object sender, CancelEventArgs e)
        {
            txtMachineName.Text = Regex.Replace(
            txtMachineName.Text,
            @"\W",  /*Matches any nonword character. Equivalent to '[^A-Za-z0-9_]'*/
            "",
            RegexOptions.IgnoreCase);
        }

        private void BtnWriteCfg_Click(object sender, EventArgs e)
        {
            WriteConfig();
        }
 
    }
}

