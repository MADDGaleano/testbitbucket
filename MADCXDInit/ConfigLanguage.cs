﻿using MADCXDInitLNG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MADCXDInit
{
    public partial class ConfigLanguage : Form
    {
        public ConfigLanguage()
        {
            InitializeComponent();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            appData.WriteXml(string.Format("{0}/data.xml", Application.StartupPath));

            string fld = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "\\";
            ResourceWriter ren = new ResourceWriter(fld + "/resource.en.resources");
            ResourceWriter res = new ResourceWriter(fld + "/resource.es.resources");
            foreach (AppData.LanguagesRow row in appData.Languages.Rows)
            {
                ren.AddResource(row.ID, row.English);
                res.AddResource(row.ID, row.Espanol);
            }
            ren.Generate(); ren.Close();
            res.Generate(); res.Close();

            this.Close();
        }

        private void ConfigLanguage_Load(object sender, EventArgs e)
        {
            try
            {
                appData.ReadXml(string.Format("{0}/data.xml", Application.StartupPath));
            }
            catch (Exception)
            {
                MessageBox.Show("Missing translations file");
            }

            dataGridView.Columns[0].ReadOnly = true;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView.Sort(this.dataGridView.Columns[0], ListSortDirection.Ascending);
        }
    }
    
}
